<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generator extends Model
{
    public static function generateIntro($headline,$subtext) {
        shell_exec("cd ../storage/ffmpeg && ./ffmpeg.sh '".$headline."' '".$subtext."'");
    }
    public static function generateCaption($headline,$subtext) {
        shell_exec("cd ../storage/ffmpeg && ./ffmpeg2.sh '".$headline."' '".$subtext."'");
    }

}
