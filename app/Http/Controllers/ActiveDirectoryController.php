<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActiveDirectoryController extends Controller
{
    public function __construct() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://login.microsoftonline.com/2ca659e4-43be-42ed-8208-33c5c2ba1910/oauth2/v2.0/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=client_credentials&client_id=5f2e6d99-c92e-4cff-8489-4abefadc69b3&client_secret=nW%5BkXG4i7g%5ByQB1@Kq9wb.y_-P_dMF95&scope=https%3A//graph.microsoft.com/.default",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Content-Type: application/x-www-form-urlencoded",
                "Cookie: stsservicecookie=ests; fpc=Aow9AXJ2-RFAnMLn83bItDOQ3_nxAQAAANE_HdYOAAAAPUzaJwEAAADyPx3WDgAAAA; x-ms-gateway-slice=estsfd"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);
        $this->accessToken = $response->access_token;
    }

    public function createUser(\App\User $user) {
        return "dangerous!";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.microsoft.com/v1.0/users",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST", 
            CURLOPT_POSTFIELDS =>"{\n  \"accountEnabled\": true,\n  \"displayName\": \"$user->name\",\n 
            \"mailNickname\": \"jfco.user$user->id\",\n  \"userPrincipalName\": \"jfco.user".$user->id."@jugendfilmcamp.de\",\n 
            \"passwordProfile\" : {\n    \"forceChangePasswordNextSignIn\": true,\n    \"password\": \"6vrA0f7".($user->id*33)."uqTBRzVnh\"\n  }\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->accessToken",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);
        return $response->id;
    }

    public function updateUsers() {
        $users = \App\User::where("team_id","!=","null")->get();
        $user_ids = [];
        foreach($users as $user) {
            $user_ids[]=$this->updateUser($user);
        }
        return $user_ids;
    }

    public function updateUser(\App\User $user) {
        $curl = curl_init();

        $user_id = $this->inviteUser($user);

        if($user_id == null) return "no_invitation for ".$user->id.$user->name;

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.microsoft.com/v1.0/users/".$user_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PATCH", 
            CURLOPT_POSTFIELDS =>"{\n   \"displayName\": \"".$user->name."\", \"givenName\": \"".$user->name."\"}",

            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->accessToken",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);

        //$user->teams_invite_redeem_url = $response->inviteRedeemUrl;
        //$user->save();
        //die(json_encode($response));
        //return $response->invitedUser->id;
        return $user_id;

    }

    public function inviteUser(\App\User $user) {
        $curl = curl_init();

        $useremail = $user->email;
        if(getenv("APP_DEBUG_AD") === true) 
            $useremail = "test.".$useremail;

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.microsoft.com/v1.0/invitations",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST", 
            CURLOPT_POSTFIELDS =>"{\n   \"invitedUserDisplayName\": \"".$user->name."\", \"invitedUserEmailAddress\": \"".$useremail."\",\n  \"inviteRedirectUrl\": \"https://teams.microsoft.com/_?lm=deeplink&lmsrc=homePageWeb&cmpid=WebSignIn#/guestLicense\"\n}",

            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->accessToken",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);
        try {
            if(!empty($response->inviteRedeemUrl))
                $user->teams_invite_redeem_url = $response->inviteRedeemUrl;
            $user->save();
            //die(json_encode($response));
            if(!empty($response->invitedUser->id))
                return $response->invitedUser->id;
            else
                return null;
        } catch(Exception $e) {
            return null;
        }
    }
    // user id 2a480dee-e395-4a39-b2d6-44acadf2ccef

    public function createGroup(\App\Team $team) {
        $users = [];
        $owners = ["https://graph.microsoft.com/v1.0/users/e11f2405-598d-4821-99c5-2c60bbf0cb93","https://graph.microsoft.com/v1.0/users/59511987-ccaf-41cf-a606-e73fea50c0b0"];

        $teamtitle= $team->title;
        $mailnick="jfco.team".$team->id;
        if(getenv("APP_DEBUG_AD") === true)  {
            $teamtitle="TEST ".$team->title;
            $mailnick="test.jfco.team".$team->id;
        }


        if($team->docent_id == 39) {
            // fitz
            $owners[] = "https://graph.microsoft.com/v1.0/users/278916fd-4e96-49c7-b6d2-c04ed8f276a0";
        } else if($team->docent_id == 70) {
            // heinz
            $owners[] = "https://graph.microsoft.com/v1.0/users/9c0f3a72-bb70-4f11-895e-ee4b61777f7c";
        } else if($team->docent_id == 11) {
            // norman
            $owners[] = "https://graph.microsoft.com/v1.0/users/b7da3070-fac4-4a49-9369-56add9f19fa8";
        } else if($team->docent_id == 4) {
            // poppe
            $owners[] = "https://graph.microsoft.com/v1.0/users/780777b2-bfc2-4ed7-934d-c8d723d6e4fe";
        }

        foreach($team->members as $user) {
            $users[] = "https://graph.microsoft.com/v1.0/users/".$this->inviteUser($user);
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.microsoft.com/v1.0/groups",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\n    \"displayName\":\"".$teamtitle."\",\n    \"mailNickname\":\"".$mailnick."\",\n 
            \"description\":\"Gruppe mit dem Thema ".$team->thema."\",\n    \"visibility\":\"Public\",\n   
            \"groupTypes\":[\"Unified\"],\n    \"mailEnabled\":true,\n    \"securityEnabled\":false,\n   
            \"members@odata.bind\":".json_encode($users)."\n,\"owners@odata.bind\":".json_encode($owners)."\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->accessToken",
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);
        return $response->id;
    }

    public function createTeam(\App\Team $team) {
        $groupId=$this->createGroup($team);
        sleep(16);
       //$groupId="5e952828-c6e1-40cc-a3a3-81232355aa79";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://graph.microsoft.com/beta/teams",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\n  \"template@odata.bind\": \"https://graph.microsoft.com/beta/teamsTemplates('standard')\",\n
            \"group@odata.bind\": \"https://graph.microsoft.com/v1.0/groups('".$groupId."')\",\n
            \"description\": \"Gruppe mit dem Thema ".$team->thema."\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $this->accessToken",
                "Content-Type: application/json"
            ),
        ));
        $response = json_decode(curl_exec($curl),0);
        curl_close($curl);
        return true;
    }

    public function getAccessToken() {
        return $this->accessToken;
    }

}
