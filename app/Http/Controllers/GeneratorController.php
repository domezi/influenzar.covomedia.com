<?php

namespace App\Http\Controllers;

use App\Generator;
use Illuminate\Http\Request;

class GeneratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generateCaption(Request $request)
    {
        request()->validate([
            "titlecaption"=>"required|min:3|max:30",
            "subtitlecaption"=>"max:40",
        ]);
        Generator::generateCaption(request("titlecaption"),request("subtitlecaption"));
        return view("assets-download",["file"=>"Bauchbinde.mp4"]);
        return response()->download("ffmpeg/Bauchbinde.mp4");
    }

    public function generateIntro(Request $request)
    {
        request()->validate([
            "title"=>"required|min:3|max:30",
            "subtitle"=>"max:40",
        ]);
        Generator::generateIntro(request("title"),request("subtitle"));
        return view("assets-download",["file"=>"Euer_Intro.mp4"]);
        return response()->download("ffmpeg/Euer_Intro.mp4");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Generator  $generator
     * @return \Illuminate\Http\Response
     */
    public function show(Generator $generator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Generator  $generator
     * @return \Illuminate\Http\Response
     */
    public function edit(Generator $generator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Generator  $generator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Generator $generator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Generator  $generator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Generator $generator)
    {
        //
    }
}
