<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportBugController extends Controller
{
    public function store() {

        $user = auth()->user();
        $data=request()->validate([
            "title"=>"required|min:5",
            "description"=>"required|min:5",
        ]);
        $data["description"] .= "\r\n\r\n================================\r\n\r\nSubmitted by ".$user->name." ".$user->email." USER-ID=".$user->id;
        $data["description"] .= "\r\n\r\nUser Agent ".$_SERVER["HTTP_USER_AGENT"]."";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://gitlab.com/api/v4/projects/17815158/issues",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "private-token: s4_2jZJL7rnf4qY8U8hs",
                "Content-Type: application/json",
                "Cookie: __cfduid=d27381c2f92643f70cbb407911431c0351586648166"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return back()->with('success', 'Vielen Dank, wir werden uns so schnell wie möglich darum kümmern.');  

    }
}
