<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function leave(Team $team)
    {
        auth()->user()->joinTeam(null);
        return view("intern.teams.leave",["user"=>auth()->user()]);
    }
    public function join(Team $team)
    {
        auth()->user()->joinTeam($team->id);
        return view("intern.teams.join",["user"=>auth()->user()]);
    }
        //
    public function index()
    {
        if(request()->wantsJson()) {
            $teams = Team::latest()->get();
            return $teams;
        } else {
            $myTeams = Team::where("docent_id",auth()->id())->get();
            $teams=Team::where("docent_id","!=",auth()->id())->orWhereNull("docent_id")->get();
            return view("intern.teams.index",compact(["myTeams","teams"]));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("intern.teams.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function patch(Team $team)
    {
        request()->validate([
            "field"=>"required",
            "value"=>""
        ]);
        $val = request("value");
        if($val == "null") $val = null;
        return \App\Team::where('id', $team->id)->update(array(request("field") =>$val));
    }


    public function store(Request $request)
    {
        $validated=request()->validate([
            "title"=>"required|min:4|max:32"
        ]);

        $team=\App\Team::create([
            "title"=>$validated["title"],
            "created_by"=>auth()->id()
        ]);

        auth()->user()->joinTeam($team->id);

        return redirect(route("teams.showOurTeam"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function showOurTeam()
    {
        return view("intern.teams.showOurTeam",["team"=>auth()->user()->team]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        return view("intern.teams.show",["team"=>$team]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function freeDocent(Team $team)
    {
        $team->docent_id=null; 
        $team->save();
        return back();
    }
    public function becomeDocent(Team $team)
    {
        $team->docent_id=auth()->id(); 
        $team->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */

    public function adminIndex() {
        $teams=\App\Team::with("docent")->with("members")->latest()->get()->map(function($team){
            $team["created_ago"]=$team->created_at->diffForHumans();
            $team["latest_upload"]=$team->uploads->last();
            $team->members->map(function($member) {$member["weeks"]  = $member->weeks; return $member;});
            return $team;
        });
        if(request()->wantsJson())
            return $teams;
        else
        return view("intern.teams.indexAdmin",["teams"=>$teams]);
    }
    public function updateThemaWunsch(Team $team)
    {
        request()->validate([
            "thema_wunsch"=>"min:3|max:255"
        ]);
        $team->thema_wunsch=request("thema_wunsch");
        $team->save();
        if(request()->wantsJson())
            return $team;
        else
            return back();
    }
    public function update(Team $team)
    {
        request()->validate([
            "thema"=>"min:3|max:255"
        ]);
        $team->thema=request("thema");
        $team->save();
        if(request()->wantsJson())
            return $team;
        else
            return back();
    }
}
