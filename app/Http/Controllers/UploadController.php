<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $search = request("search");

        if(substr($search,0,5) == "tags:")
            return redirect(route("uploads.byTags",substr($search,5)));

        if(substr($search,0,4) == "tag:")
            return redirect(route("uploads.byTag",substr($search,4)));

        $uploads = Upload::where("tags","like","%".$search."%")->orWhere("genre","like","%".$search."%")->orWhere("title","like","%".$search."%")->with("uploader")->latest()->get();
        return view("intern.uploads.search",compact("uploads","search"));
    }

    public function indexByTags($tags)
    {
        $uploads = Upload::query();
        foreach(explode(",",str_replace($tags," ","%")) as $tag) {
            $uploads->orWhere("tags","like","%".$tags."%");
        }
        $uploads = $uploads->distinct()->with("uploader")->latest()->get();

        $search="tags:".$tags;
        return view("intern.uploads.search",compact("uploads","search"));
    }


    public function indexByTag($tag)
    {
        $uploads = Upload::where("tags","like","%".$tag."%")->with("uploader")->latest()->get();
        $search="tag:".$tag;
        return view("intern.uploads.search",compact("uploads","search"));
    }


    public function patch(\App\Upload $upload)
    {
        if(\Gate::allows("docent") || \Gate::allows("admin") ||  ($upload->uploaded_by == auth()->id() && (
            request("status") == "archive" || request("credits") != null 
        ) )) {
            if(request("status") != null)
                $upload->status = request("status");
            else if(request("credits") != null)
                $upload->credits = request("credits");
            else if(request("description") != null)
                $upload->description = request("description");
            $upload->save();
        }
        return back();

    }

    public function unlistedPublish()  {
        $uploads = \App\Upload::where("status","publish")->get();
         return view("intern.uploads.unlisted-publish", compact("uploads")); 
    }


    public function index(\App\Team $team)
    {
        $uploads = Upload::where("team_id",$team->id)->with("uploader")->latest()->get();
        return view("intern.uploads.index",compact("uploads","team"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(\App\Team $team)
    {
        return view("intern.uploads.create",compact("team"));
    }
    public function createBTS(\App\Team $team)
    {
        return view("intern.uploads.createBTS",compact("team"));
    }
    public function createStock(\App\Team $team)
    {
        return view("intern.uploads.createStock",compact("team"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Team $team, Request $request)
    {
        $validated = request()->validate([
            "title"=>"required|min:5|max:64",
            "location"=>"required|min:5|max:64",
            "genre"=>"required|min:3|max:32",
            "url_final"=>"url|max:256",
            "url_raw"=>"url|required|min:10|max:256",
            "tags"=>"required|min:30|max:512"
        ]);
        if(!isset($validated["url_final"]))
        $validated["url_final"]="";
        $validated["tags"]=str_replace([" ,"," , "," ",", "],",",$validated["tags"]);
        $validated["tags"]=str_replace([" ,"," , "," ",", "],",",$validated["location"]).",||,".$validated["tags"];
        unset($validated["location"]);
        $validated["team_id"]=$team->id;
        $validated["credits"]=implode(", ",$team->members->pluck("name")->toArray());
        $validated["uploaded_by"]=auth()->id();

        $upload = Upload::create($validated);
        return redirect(route("uploads.index",$team->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function show(Upload $upload)
    {
        //
        $team = $upload->team;
        return view("intern.uploads.show",compact("upload","team"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function edit(Upload $upload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Upload $upload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Upload  $upload
     * @return \Illuminate\Http\Response
     */
    public function destroy(Upload $upload)
    {
        //
    }
}
