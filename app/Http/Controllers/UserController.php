<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function delete(\App\User $user) {
        if($user->team_id == null && $user->is_admin == 0 && $user->is_docent == 0 && $user->enabled == 0)
            return $user->delete();
        else
            return false;
    }
    public function show(\App\User $user) {
        return view("intern.users.show",compact("user"));
    }
    public function patchSelf() {
        $user=auth()->user();
        if(request("field") !== "weeks") {
            $inputs = request()->all();
            $out=[];
            $out["bio"]=request("bio");
            $skills=[];
            foreach($inputs as $key=>$input) {
                if(substr($key,0,6) == "skill_")
                    $skills[] = $key;
            }
            $out["skills"]=$skills;
            $user->bio = json_encode($out);
            $user->save();
            return back()->with('success', 'Deine Skills und Bio wurden erfolgreich geupdated.');  

        } else {
            $weeks = request("weeks");
            if($weeks == null)
                $weeks = [];
            $user->weeks()->sync(array_keys($weeks));
            return back()->with('success_weeks', 'Deine Wochen wurden erfolgreich geupdated.');  
        }
    }
    public function patch(\App\User $user) {
        request()->validate([
            "field"=>"required",
            "value"=>""
        ]);
        if(request("field") == "enabled")
            return $this->setEnabled($user, request("value"));
        else
            \App\User::where('id', $user->id)->update(array(request("field") => request("value")));
    }
    public function setEnabled(\App\User $user, $enabled) {
        $user->enabled = $enabled;
        $user->save();
        if($enabled == "1")
            \Notification::route('slack', env('SLACK_HOOK'))->notify(new \App\Notifications\InfluAssiNotification(":heavy_check_mark: User *".$user->name."* wurde von ".auth()->user()->name." *aktiviert*."));
        else
            \Notification::route('slack', env('SLACK_HOOK'))->notify(new \App\Notifications\InfluAssiNotification(":x: User *".$user->name."* wurde von ".auth()->user()->name." *DEAKTIVIERT*."));
    }
    public function index() {
        $users=\App\User::with("team")->with("weeks")->latest()->get()->map(function($user){
            $user["created_ago"]=$user->created_at->diffForHumans();
            return $user;
        });
        if(request()->wantsJson())
            return $users;
        else
        return view("intern.users.index",["users"=>$users]);
    }
}
