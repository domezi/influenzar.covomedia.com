<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function store() {
        $validated=request()->validate([
            "title"=>"required",
            "youtube_id"=>"required",
            "team_id"=>"required|exists:teams,id",
        ]);

        $validated["thema"]=\App\Team::findOrFail($validated["team_id"])->thema;

        $video=\App\Video::create($validated);

        return redirect(route("videos.index"));
    }

    public function delete(\App\Video $video) {
        $video->delete();
        return redirect(route("videos.index"));
    }


    public function show(\App\Video $video) {
        return view("videos.show",compact("video")); 
    }


    public function feed() {
        return view("feed",[
            "videos"=>\App\Video::all()
        ]);
    }

    public function index() {
        return view("videos.index",[
            "videos"=>\App\Video::all()
        ]);
    }

    public function create() {
        return view("videos.create",[
            "teams"=>\App\Team::all()
        ]);
    }
}
