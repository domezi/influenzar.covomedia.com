<?php

namespace App\Http\Middleware;

use Closure;

class WriteLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // log
        if(\Route::currentRouteName()!=null && auth()->id() != 6 && !$request->wantsJson() && $_SERVER["HTTP_USER_AGENT"]!="ELB-HealthChecker/1.0")
            \App\Log::create([
                    "phpsessid"=>session()->getId(),
                    "method"=>$_SERVER['REQUEST_METHOD'],
                    "route"=>\Route::currentRouteName(),
                    "path"=>\Request::getRequestUri(),
                    "user_agent"=>substr($_SERVER["HTTP_USER_AGENT"],0,300),
                    "user_id"=>auth()->id()
            ]);
?>

<?php
        if(\Gate::allows('admin') && !$request->wantsJson()) {
?>
<style>
body {
margin-bottom:49px !important;
}
</style>
<nav class="stats-bar navbar navbar-expand-lg" style="display:none">
    <a class="navbar-brand" href="<?php echo route("metrics.live");?>">
        <i class="fa fa-signal"></i>
        Live-Metrics 
    </a>

  <button class="navbar-toggler" type="button" style="color:#ddd" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fa fa-plus"></i>
    einblenden
  </button>
 <?php
            $db=mysqli_connect(getenv("DB_HOST"),getenv("DB_USERNAME"),getenv("DB_PASSWORD"),getenv("DB_DATABASE"));
            $users=["docents"=>[],"admins"=>[],"teamers"=>[]];

            //$tmp = $db->query("SELECT max(l.created_at) as last_action, u.name, u.id FROM logs l, users u WHERE u.id = l.user_id and u.id != '".auth()->id()."' and l.created_at > '".date("Y-m-d H:i:s",strtotime("-10 min"))."' GROUP BY u.id ORDER BY last_action asc LIMIT 99");
            $tmp = $db->query("SELECT max(l.created_at) as last_action, u.name, u.id FROM logs l, users u WHERE  u.is_docent = 1 and u.id = l.user_id  and l.created_at > '".date("Y-m-d H:i:s",strtotime("-10 min"))."' GROUP BY u.id ORDER BY last_action asc LIMIT 99");
            if($tmp) $users["docents"]=$tmp->fetch_all();

            $tmp = $db->query("SELECT max(l.created_at) as last_action, u.name, u.id FROM logs l, users u WHERE  u.is_admin = 1 and u.id = l.user_id  and l.created_at > '".date("Y-m-d H:i:s",strtotime("-10 min"))."' GROUP BY u.id ORDER BY last_action asc LIMIT 99");
            if($tmp) $users["admins"]=$tmp->fetch_all();

            $tmp = $db->query("SELECT max(l.created_at) as last_action, u.name, u.id FROM logs l, users u WHERE  u.is_docent = 0 and u.is_admin = 0 and u.id = l.user_id  and l.created_at > '".date("Y-m-d H:i:s",strtotime("-10 min"))."' GROUP BY u.id ORDER BY last_action asc LIMIT 99");
            if($tmp) $users["teamers"]=$tmp->fetch_all();

            $latest_guests = $db->query("SELECT max(l.created_at) as last_action FROM logs l WHERE user_id is null and l.created_at > '".date("Y-m-d H:i:s",strtotime("-10 min"))."'
                 GROUP BY phpsessid ORDER BY last_action asc LIMIT 99");
            if($latest_guests) $guests=$latest_guests->fetch_all(); else $guests = [];
 ?>
            
  <div class=" collapse navbar-collapse" id="navbarSupportedContent1">
      <ul class="navbar-nav mr-auto">

          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo count($users["admins"]);?> Administratoren
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php
            foreach($users["admins"] as $user) {


                $date1 = new \DateTime();
                $date2 = new \DateTime($user[0]);
                $interval = $date1->diff($date2);

                echo '<a class="dropdown-item" href='.route("metrics.user",$user[2]).'>'.$user[1]." ".
                    "<span class=text-muted>".$interval->format("%imin, %s s")."</span>".
                    '</a>';
            }
?>
              </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo count($users["docents"]);?> Dozenten
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php
            foreach($users["docents"] as $user) {


                $date1 = new \DateTime();
                $date2 = new \DateTime($user[0]);
                $interval = $date1->diff($date2);

                echo '<a class="dropdown-item" href='.route("metrics.user",$user[2]).'>'.$user[1]." ".
                    "<span class=text-muted>".$interval->format("%imin, %s s")."</span>".
                    '</a>';
            }
?>
              </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo count($users["teamers"]);?> Teilnehmer
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php
            foreach($users["teamers"] as $user) {


                $date1 = new \DateTime();
                $date2 = new \DateTime($user[0]);
                $interval = $date1->diff($date2);

                echo '<a class="dropdown-item" href='.route("metrics.user",$user[2]).'>'.$user[1]." ".
                    "<span class=text-muted>".$interval->format("%imin, %s s")."</span>".
                    '</a>';
            }
?>
              </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo count($guests);?> Gäste
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php
            foreach($guests as $user) {


                $date1 = new \DateTime();
                $date2 = new \DateTime($user[0]);
                $interval = $date1->diff($date2);

                echo '<a class="dropdown-item">Gast '.
                    "<span class=text-muted>".$interval->format("%imin, %s s")."</span>".
                    '</a>';
            }
?>
              </div>
          </li>
          <li class="nav-item ">
              <a class="nav-link" href="<?php echo route("metrics.route",\Route::currentRouteName());?>" >
                  Diese Seite: <?php echo $db->query("select count(*) as count from logs where route = '".\Route::currentRouteName()."'")->fetch_object()->count;?> Aufrufe <i class="fa fa-caret-right"></i>
              </a>
          </li>
          <li class="nav-item ">
              <a class="nav-link" href="<?php echo route("metrics.live");?>">
                  Alle Seiten: <?php echo $db->query("select count(*) as count from logs where 1")->fetch_object()->count;?> Aufrufe <i class="fa fa-caret-right"></i>             
              </a>
          </li>

      </ul>
  </div>
</nav>
<?php
        }
        return $next($request);
    }
}
