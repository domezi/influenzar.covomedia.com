<?php

namespace App\Http\Middleware;

use Closure;

class isDocent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->is_docent == '1') {
            return $next($request);
        }

        return redirect('home');

    }
}
