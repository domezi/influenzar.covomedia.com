<?php

namespace App\Http\Middleware;

use Closure;

class isTeamer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->is_docent == '0' && \Auth::user()->is_admin == '0') {
            return $next($request);
        }

        return redirect('home');
    }
}
