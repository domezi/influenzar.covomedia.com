<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\SlackMessage;

class InfluAssiNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
$this->message = $message;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toTeams($notifiable)
    {


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getenv("TEAMS_HOOK"));
        // SSL important
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode(["text"=>$this->message]));
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json'));


        $output = curl_exec($ch);
        curl_close($ch);


        //$this -> response['response'] = json_decode($output);
        //
        return true;

    }
    public function toSlack($notifiable)
    {
        $this->toTeams($notifiable);
        return (new SlackMessage)
                ->from('Ghost', ':ghost:')
                ->to('#user-freischalten')
                ->content($this->message);
    }

}
