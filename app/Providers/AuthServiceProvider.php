<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //
        //
        Gate::define('teamer', function ($user) {
            return $user->is_docent == "0" && $user->is_admin == "0";
        });
        Gate::define('admin', function ($user) {
            return $user->is_admin == "1";
        });
        Gate::define('docent', function ($user) {
            return $user->is_docent == "1";
        });
    }
}
