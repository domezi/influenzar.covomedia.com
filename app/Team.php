<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded = [];

    public function members() {
        return $this->hasMany(User::class);
    }

    public function tasks() {
        return null;
    }

    public function uploads() {
        return $this->hasMany(\App\Upload::class);
    }
    public function docent() {
        return $this->belongsTo(User::class,"docent_id");
    }
}
