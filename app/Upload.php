<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $guarded = [];
    public function team() {
        return $this->belongsTo(Team::class);
    }
    public function uploader() {
        return $this->belongsTo(User::class,"uploaded_by");
    }
}
