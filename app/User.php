<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    public function joinTeam($team_id) {
        $this->team_id = $team_id;
        $this->save();
    }

    public function hasSkill($tag) {
        return strstr($this->bio,$tag);
    }
    public function getBioText() {
        $bio = json_decode($this->bio);
        if($bio == null)
            return "";
        else return $bio->bio;
    }
    public function hasTeam() {
        return $this->team_id>0;
    }

    public function uploads() {
        return $this->hasMany(Upload::class,"uploaded_by");
    }

    public function weeks() {
        return $this->belongsToMany(Week::class);
    }
    public function team() {
        return $this->belongsTo(Team::class);
    }
    public function weeksInFuture() {
        return $this->weeks->filter(function($week) {return strtotime($week->start) > time();});
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
