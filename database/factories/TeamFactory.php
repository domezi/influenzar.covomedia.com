<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    $user=factory(\App\User::class)->create();

    $team=Team::create([
        "title"=>"Team ".$faker->word,
        "created_by"=>$user->id,
        "docent_id"=>factory(\App\User::class)->create(["is_docent"=>true])->id
    ]);
    $user->team_id=$team->id;
    $user->save();
    factory(\App\User::class,2)->create(["team_id"=>$team->id]);
    return true;
});
