<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string("title");
            $table->string("thema")->default("Ihr habt noch kein Thema zugeteilt bekommen.");
            $table->string("thema_wunsch")->default("Ersetzt diesen Text durch eure 3 Themenwünsche."); // patched manually on server
            $table->unsignedBigInteger("docent_id")->nullable();
            $table->unsignedBigInteger("created_by");
            $table->timestamps();

            $table->foreign("created_by")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("docent_id")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
