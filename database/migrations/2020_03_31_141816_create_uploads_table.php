<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->id();
            $table->string("title",64);
            $table->string("genre",32);
            $table->string("url_final",256);
            $table->string("url_raw",256);
            $table->string("tags",512);
            $table->unsignedBigInteger("uploaded_by");
            $table->unsignedBigInteger("team_id");
            $table->timestamps();

            $table->foreign("uploaded_by")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("team_id")->references("id")->on("teams")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
