@extends('layouts.app')

@section('content')
<div class="container def">
        <h1 style="">
            Assets
        </h1>
        <div class="assets" style="color:white; white-space:pre-line">
            <div class="link mb-4 mt-4">
                <h2>Sponsoren & Logos</h2>
                <a href="{{asset('ffmpeg/Intro_Abspann.zip')}}" class="btn btn-primary">Intro & Abspann downloaden</a>
            </div>

            <div class="link mb-2">
                <h2>Titelvideo erstellen</h2>
                <form onSubmit="document.getElementById('hide-on-load').style.display='none';document.getElementById('loading').style.display='block'"
                    method="post" action="{{route("generateIntro")}}">
                    @csrf
                    <div id="hide-on-load">
                    @error("title")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <input required class="form-control" value="{{old("title")}}" name="title" placeholder="Titel eures Videos">
                    @error("subtitle")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <input required class="form-control" value="{{old("subtitle")}}" name="subtitle" placeholder="Unterüberschrift eures Videos">
                    <input type="submit" value="Titelvideo erstellen" class="btn btn-primary">
                    </div>
                    <div style="display:none" id="loading">
                        <div class="text-primary"><i class="fa fa-spin fa-spinner"></i> Bitte warte, dies kann einige Sekunden dauern ...</div>
                        <div>
                            Sobald Dein Video fertig ist:
                            1) Drücke Strg + S und speichere die Datei
                            2) Setze Dein neues Intro an den Anfang Deines Videos
                        </div>
                    </div>
                </form>
            </div>

            <div class="link mb-2">
                <h2>Bauchbinde erstellen</h2>
                <form onSubmit="document.getElementById('hide-on-load-caption').style.display='none';document.getElementById('loading-caption').style.display='block'"
                   method="post" action="{{route("generateCaption")}}">
                    @csrf
                    <div id="hide-on-load-caption">
                    @error("titlecaption")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <input required class="form-control" value="{{old("titlecaption")}}" name="titlecaption" placeholder="Name der Person">
                    @error("subtitlecaption")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <input required class="form-control" value="{{old("subtitlecaption")}}" name="subtitlecaption" placeholder="Tätigkeit/Position der Person">
                    <input type="submit" value="Bauchbinde erstellen" class="btn btn-primary">
                    </div>
                    <div style="display:none" id="loading-caption">
                        <div class="text-primary"><i class="fa fa-spin fa-spinner"></i> Bitte warte, dies kann einige Sekunden dauern ...</div>
                        <div>
                            Sobald Dein Video fertig ist:
                            1) Drücke Strg + S und speichere die Datei
                            2) Setze im Schnittprogramm einen Chroma Key (Farbe Schwarz) -> Dein Video wird dadurch transparent
                        </div>
                    </div>
                </form>
            </div>

            <div class="link mb-2">
                <h2>Bauchbinde im Schnittprogramm verwenden</h2>
                <div class="row mt-3">
                    <div class="col-md-4">
                        <h4 style="color:orange">DaVinci Resolve</h4>
                        <iframe width="300" height="180" src="https://www.youtube.com/embed/adDnxWQuaPk?start=240" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-4">
                        <h4 style="color:orange">Premiere Pro</h4>
                        <iframe width="300" height="180" src="https://www.youtube.com/embed/IeIBXj_beDk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-4">
                        <h4 style="color:orange">Final Cut Pro</h4>
                        <iframe width="300" height="180" src="https://www.youtube.com/embed/-59r7WPZoO0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </div>

</div>
@endsection
