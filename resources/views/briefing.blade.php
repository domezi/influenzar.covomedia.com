@extends('layouts.app')

@section('content')
<div class="container def">
        <h1>Briefing</h1>
        <div class="themen">
            <hr>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/LWo4H8Dk4vY?controls=1&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <h2>
            Die Teams
        </h2>
Ihr bildet selbständig Teams.
Ein Team kann aus einer oder mehreren Personen bestehen.
Dabei ist es völlig egal, von wo die einzelnen Teammitglieder kommen.
Sprich: Die Teammitglieder können auch verstreut leben.
<h2>
    Themen
</h2>
Jedes Team bekommt mindestens ein Thema zugeteilt, zu welchem es recherchiert und Filme
produziert.
<br>
<br>

        <a href="{{route('themen')}}" class="btn btn-lg btn-primary">Themen <i class="fa fa-external-link"></i></a>
<h2>
    Ansprechpartner
</h2>
Jedes Team bekommt einen Ansprechpartner vom Jugendfilmcamp zur Seite gestellt.
Wochenweise werden die Ansprechpartner gewechselt.
Die Teams bleiben also bestehen, jedoch bekommt ihr immer neue Sichtweisen und Anregungen
durch die wechselnden Ansprechpartner.
<br>
<br>

        <a href="{{route('team')}}" class="btn btn-lg btn-primary">Ansprechpartner <i class="fa fa-external-link"></i></a>
<h2>
    Die Machart
</h2>
Alles ist offen.
Es gibt keine Vorgaben.
Jedes Genre ist willkommen.
Dokumentation, Interviews, Komödie, Krimi, Animation...
Macht was ihr wollt - nur macht es!
Länge der Filmbeiträge 1 bis max. 3 min.
Die Länge der Filme sollte internettauglich sein.
Sprich: überprüft Euer eigenes Internetverhalten - niemals zu lang - lieber knackig und spannend.
<h2>
    Filmformat
</h2>
Immer Querformat!
Egal ob mit Handy oder ProfiKamera.
In Ausnahmefällen - falls es künstlerisch nötig ist oder externes Material eingespielt wird -
natürlich auch mal Hochformat.
<h2>
    Wie oft
</h2>
Jedes Team produziert pro Woche einen Beitrag zu seinem Thema.
<h2>
    Welche Länder
</h2>
Deutschland, Österreich, Schweiz und hoffentlich viele Länder Europas.
Begeistert Freunde & Filmfreaks aus anderen europäischen Ländern sich zu beteiligen.
<h2>
    Vorspann, Abspann, Layout
</h2>
Den Vorspann & Abspann für eure Filme & Themen findet ihr unter WICHTIGE LINKS.
Dort findet ihr auch Hinweise zum Erstellen von Texttafeln.
(Bitte verzichtet auf ausufernde Nennung aller Teammitglieder in euren Beiträgen.)
<br>
<br>
        <a href="{{route('assets')}}" class="btn btn-lg btn-primary">Vorspann & Abspann <i class="fa fa-external-link"></i></a>
<h2>
    Einsenden der Filme
</h2>
Die fertigen Filme ladet ihr auf folgende Datenbank:
Link Datenbank
Bitte die Filme immer in zwei Varianten einsenden:<br>
1x Film mit Musik<br>
1x Film ohne Musik<br>
Aus ausgewählten Beiträgen wird nach Abschluss des Projektes eine Langzeitdoku erstellt, daher
benötigen wir eure Beiträge auch ohne Musik.     
<h2>
    Au&szlig;endarstellung
</h2>
Die fertigen Filme werden nach Themen sortiert auf einer eigenen Website gestreamt
Zudem werden ausgewählte Beiträge über Social-Media-Kanäle präsentiert.
Der Link zur Website folgt in Kürze.
        </div>
        <br><br>
        <a href="{{route('links')}}" class="btn btn-lg btn-primary">Wichtige Links <i class="fa fa-external-link"></i></a>
        <a href="{{route('register')}}" class="btn btn-lg btn-primary">Jetzt Team anmelden <i class="fa fa-chevron-right"></i></a>
</div>
@endsection
