@extends('layouts.app')

@section('content')
<div class="container def">
        <h1 style="">
            FAQ: Häufige Fragen
        </h1>
     
        <div class="assets" style="color:white; white-space:pre-line"> <h2>
                Was ist Influenzar 2020?
            </h2> Influenzar 2020 ist ein Projekt des Jugendfilmcamps Arendsee und der <a href="https://www.fes.de" target="_blank" > <i class="fa fa-external-link"></i> Friedrich-Ebert-Stiftung </a>, von, mit und für unsere junge Generation. Du produzierst mit unserer Unterstützung allerlei Kurzfilme, Interviews, Berichte, etc. zu Themen unserer Zeit.
Getreu dem Motto «Vernetzt euch, Filmarbeit ist Teamarbeit», auch zu schwierigen Zeiten, gerade, weil es genau jetzt nötig ist. Wir sind die Zukunft. Die Zukunft beginnt jetzt.
<h2>
    Wie melde ich mich an?
</h2> Am Ende des F.A.Q.s siehst du einen Button, über welchen du dich registrieren kannst. Gib deine Daten ein, und schon geht’s los.
<h2>
    Bis wann kann ich mich anmelden?
</h2>  Du kannst dich jederzeit anmelden und dann im Startbereich dein Profil ausfüllen und dich verbindlich für eine oder mehrere Wochen eintragen. Die Wochen fangen jeden Mittwoch am Nachmittag an, die Wochenauswahl und dein Profil solltest du deshalb bis Montag um 14:00 ausgefüllt haben, um an der nächsten Woche teilzunehmen. 
<h2>
    Wie viel Zeit sollte ich mitbringen?
</h2> Auch wenn das Projekt online stattfindet, bedenkt, dass es mit einem Zeitaufwand verbunden ist. Ihr müsst selbst wissen, ob das für euch neben Studium, Schulaufgaben oder Home Office möglich ist.
Es gibt natürlich Aufgaben in Teams die mehr oder weniger Zeit in Anspruch nehmen. Sprecht euch da gemeinsam einfach gut ab. Der Wochenbeginn nachmittags/abends am Mittwoch steht als Termin fest, alles weitere klärt ihr mit den Dozent*innen und mit eurem Team.
<h2>
    Wie erstelle ich ein Team?
</h2> <ol>
    <li>Registriere dich</li>
    <li>Warte, bis wir Dich freigeschaltet haben</li>
    <li>Klicke auf "Mein Team"</li>
    <li>Klicke "Team erstellen"</li>
</ol>Jetzt bekommst Du einen Link angezeigt. Mit diesem Link können Deine Freunde dem Team beitreten (Sobald sie die Schritte 1. und 2. erledigt haben).
<h2>
    Wie lade ich meine Freunde in mein Team ein?
</h2> Logge Dich ein, und klicke auf "Mein Team".
Jetzt bekommst Du einen Link angezeigt. Mit diesem Link können Deine Freunde dem Team beitreten (Sobald sie die Schritte 1. und 2. erledigt haben).
<h2>
    Wie trete ich einem Team bei?
</h2> Registriere Dich und warte auf Deine Freischaltung. Fordere dann den Einladungslink von eurem*r Teamsprecher*in an. Mit diesem Link trittst Du dem Team bei.
<h2>
    Ich habe kein Team, kann ich mich trotzdem anmelden?
</h2> Klar, da spricht nichts dagegen. Du kannst uns gerne zeigen, was für ein cooles Projekt du als One-Woman/Man-Show auf die Beine stellst. Oder vielleicht findest du noch ein Team, das genau in dem Fachgebiet noch Hilfe brauchen kann, in welchem du richtig stark bist.
<h2>
    Wie wählen wir unser Thema?
</h2> Nach der Anmeldung könnt ihr euch drei unserer Themen auswählen, von welchen euch dann eines zugewiesen wird.
<h2>
    Werden wir von euch bei der Produktion unterstützt?
</h2> Jedes Team bekommt einen erfahrenen Dozenten zugeteilt, der euch mit Tat und Kraft unterstützt. Zurzeit sind das Matthias Poppe, Heinz Neumann und Fitz van Thom. Dazu steht euch das Jugendteam ebenfalls fast rund um die Uhr als Ansprechpartner zur Verfügung.
<h2>
    Bis wann muss mein Team den fertigen Film einsenden?
</h2> Angedacht ist eine Produktionszeit über eine Woche, genau wie im Jugendfilmcamp. Genaue Deadlines werden in Absprache mit den Dozenten noch kommuniziert.
<h2>
    Wie sende ich euch unseren fertigen Film?
</h2> Informationen dazu gibt es nach der Anmeldung und Teambildung. Die Filme werden direkt über die Webseite hochgeladen.
<h2>
    Wir haben eine Frage, an wen sollen wir uns wenden?
</h2> Unter «Ansprechpartner» findet ihr unsere JUGEND Ansprechpartner*innen mitsamt Kontaktinfo. Gerne helfen wir euch bei sämtlichen Fragen.
<br>
<br>
<a href="{{route("register")}}" class="btn btn-primary">Registrieren & Team erstellen/beitreten</a>
<a href="{{route("team")}}" class="btn btn-primary">Ansprechpartner</a>
<br>
<br>
<br>

        </div>

</div>
@endsection
