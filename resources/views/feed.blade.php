@extends('layouts.app')

@section('content')
<div class="container" style="background-size:cover;background-image:url( {{asset("img/home/blured.jpg")}} );padding:0;">
    <img src="{{asset("img/home/feed.jpg")}}" width="100%">
    <div style="padding:20px 30px;">
        <div class="text-center">
            <h1 style="color:white;margin-top:25px">Das Projekt</h1>
            <img src="https://jugendfilmcamp.de/images/layout/underline.svg" width=600>
        </div>
        <div class="row home-row">
            <div class="col-lg-5 mt-4">

                <div onClick="showVideo('WlaEFhtS27Y')" style="background-image:url(https://img.youtube.com/vi/WlaEFhtS27Y/maxresdefault.jpg)" class="video-thumb xl">
                    <img src="https://jugendfilmcamp.de/images/player/videothumbplay.svg">
                </div>
                <br>

            </div>
            <div class="col-lg-7">
                <p class="ml-md-4" style="color:white;white-space:pre-line;padding-top:23px">Frühjahr 2020 – und nichts ist wie vorher. Die ganze Welt
                ist im Krisenmodus, das Coronavirus greift überall und bei jedem tief in die vertraute Lebenssituation ein. Über Jahrzehnte gewonnene 
                Sicherheiten lösen sich schlagartig auf, aber auch neue, bislang kaum realistische Perspektiven werden am Horizont deutlich. Auf diese 
                Situation wollen wir jetzt reagieren: Schnell, jung, kreativ, authentisch, kritisch und visionär. Krise ist Chance! 

                Wir legen mit unseren Mitteln Zeugnis ab von diesem einmaligen Moment unseres Lebens. So entsteht ein filmisches Zeitdokument einer sich im Großen,
                wie im Kleinen rasant wandelnden Gesellschaft, Hoffnung inklusive.

                InfluenZAR 2020 ist ein Projekt des <a href="https://www.jugendfilmcamp.de/" target="_blank" class=""><i class="fa fa-external-link"></i> Jugendfilmcamp Arendsee</a> und
                der <a href="https://www.fes.de/" target="_blank" class=""><i class="fa fa-external-link"></i> Friedrich Ebert Stiftung</a>.<br>
                Hier <a href="{{route("fes")}}" target="_blank"><i class="fa fa-external-link"></i> mehr Infos</a> zum Forum Politik und Gesellschaft der Friedrich-Ebert-Stiftung.
                <br/>
                
                <p>
            </div>
        </div>
        <br/>


    <div class="video-cards">
        <div class="text-center mb-5">
            <h1 style="color:white;margin-top:25px">Die Filme</h1>
            <img src="https://jugendfilmcamp.de/images/layout/underline.svg" width=600>
            <p class="my-2">Hier können Sie unsere aktuellen Filme ansehen</p>
        </div>

        @forelse($videos as $video)
            <div class="video-card" onClick="showVideo('{{$video->youtube_id}}')" >
                <div style="background-image:url(https://img.youtube.com/vi/{{$video->youtube_id}}/maxresdefault.jpg)" class="video-thumb">
                    <img src="https://jugendfilmcamp.de/images/player/videothumbplay.svg">
                </div>
                <p class="time-ago">{{$video->created_at->diffForHumans()}}</p>
                <h4>{{$video->title}}</h4>
                <p>{{$video->thema}}</p>
            </div>
        @empty
        <p class="text-muted">Es wurden noch keine Videos hochgeladen.</p>
        @endforelse

    </div>

    </div>

    <div id="fullscreen-video" style=display:none onClick="hideVideo()">
        <div id="wrap-spinner"><i class="fa fa-spin fa-spinner"></i></div>
        <div id="video-close-wrapper">
            <i class="fa fa-times" onClick="hideVideo()"></i>
            <br>
            <iframe id="fullscreen-video-iframe" width="900" height="507" src="https://www.youtube.com/embed/3VMjLI8pxIw" 
                                                                          frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>


</div>
@endsection
