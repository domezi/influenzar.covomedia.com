@extends('layouts.app')

@section('content')
<div class="container def">
     
        <h1 style="">Forum Politik und Gesellschaft</h1><h2 class="text-primary">Friedrich-Ebert-Stiftung</h2>
        <br>
        <img src="{{asset("img/fes/fes.jpg")}}" width=100%>
        <br>
        <br>
        <br>
        <div class="assets" style="color:white; ">
        Das Forum Politik und Gesellschaft der FES arbeitet u.a. mit und für
Jugendliche. Mitmachen, mitreden, mitentscheiden: Ziel ist es, Lust auf
Beteiligung zu wecken, Partizipation zu fördern und Engagement zu
unterstützen. Wie funktioniert Politik? Wie sind demokratische Prozesse
organisiert? Wo kann man sich einbringen? Junge Menschen sollen
Instrumente an die Hand bekommen, mit denen sie ihrer Stimme Gehör
verschaffen können. Planspiele bringen Jugendlichen politische Abläufe
näher. Kompetenztrainings und Workshops vermitteln ihnen wichtige
Fähigkeiten. Beteiligungsorientierte Veranstaltungsformate
ermöglichen den Jugendlichen, eigene Standpunkte zu entwickeln und
mit politischen Entscheidungsträger_innen zu diskutieren.  
<br><br>

Im Rahmen von Konferenzen und Fachgesprächen lädt das Forum Politik und
Gesellschaft darüber hinaus Entscheidungsträger_innen,
Multiplikator_innen, Fachpublikum und die interessierte Öffentlichkeit
ein, über aktuelle jugendpolitische Themen in einen Dialog zu treten.
Wir möchten mit unserer Arbeit dazu beitragen, gesellschaftspolitische
Interessen und Erwartungen junger Menschen in den öffentlichen Diskurs
einzubringen. Junge Perspektiven sollen sichtbar gemacht und bei der
Vernetzung unterstützt werden. 
Ach ja und in Zeiten von Kontaktsperren lassen wir den Diskussionsstoff
einfach per Film ganz kontaktlos nach Hause liefern. 
<br><br>
<a href="https://www.fes.de" class="btn btn-primary">
    <i class="fa fa-external-link"></i>
    Friedrich-Ebert-Stiftung
</a>
<a href=" https://www.fes.de/themenportal-gender-jugend/jugend " class="btn btn-primary">
    <i class="fa fa-external-link"></i>
Jugendportal
</a>

<br>
<br>
        </div>

</div>
@endsection
