@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Hey {{explode(" ",auth()->user()->name)[0]}} !</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <br>
                    <h4 class="text-warning">Die nächste Online-Camp Woche beginnt am <u>Mi, den 15. April</u>!</h4>
                    <p class="text-warning">Bitte trage Dich nur ein, wenn Du am Mittwoch nachmittag/abend Zeit für die Teambesprechungen hast.</p>
                    <br>

                    @can("teamer")
                    <p>Hier werden bald die ersten Themenvorschläge oder Aufgaben von euren Dozenten*innen aufgelistet.</p>
                    @endcan

                    @can("docent")
                    <p>Möchtest Du zu Deinen Teams? Klicke hier: <a href="{{route("teams.index")}}">Meine Teams</a></p>
                    @endcan

                    @if(auth()->user()->enabled)
                    <a href="{{route("briefing")}}" class="btn btn-primary">Briefing</a>
                    <a href="{{route("themen")}}" class="btn btn-primary">Themen</a>
                    <a href="{{route("assets")}}" class="btn btn-primary">Assets</a>
                    @endif
                    <a href="{{route("team")}}" class="btn btn-primary">Ansprechpartner</a>
                    <a href="{{route("faq")}}" class="btn btn-primary">FAQ</a>



                </div>
            </div>
            <div class="card">
                <div class="card-header sm">Meine Wochen</div>
                <div class="card-body">
                    @if (\Session::has('success_weeks'))
                    <div class="alert alert-success">
                        <i class="fa fa-check"></i>
                        {!! \Session::get('success_weeks') !!}
                    </div>
                    @endif
                    <p> Ich bin/möchte Teilnehmer folgender Wochen sein: </p>
                    <form method="post" action="{{route("user.patch")}}">
                        @method("patch")
                        @csrf
                        <input type="hidden" name="field" value="weeks">
                        @forelse(\App\Week::all() as $week) 
                            <input type="checkbox" 
                                @if(strtotime($week->start." 00:00:00") < time())
                                    disabled style="opacity:.4"
                                @endif
                                class="mt-1 custom-checkbox" name="weeks[{{$week->id}}]" 
                                @if(auth()->user()->weeks->contains($week)) 
                                    checked="true"
                                @endif
                                value="true"> 

                                @if(strtotime($week->start." 00:00:00") < time())
                                <span class="text-muted"> {{$week->title}} </span>
                                @else
                                    {{$week->title}}
                                @endif
                            <span class="text-muted">{{date("d.m",strtotime($week->start))." - ".date("d.m.y",strtotime($week->end))}}</span><br>
                        @empty
                        <p class="text-muted">Keine Wochen verfügbar aktuell.</p>
                        @endforelse
                        <input type="submit" class="my-3 btn btn-primary" value="Wochenauswahl speichern">
                    </form>
                    <i>Die Teilnahme ist kostenlos, eine Anmeldung jedoch verpflichtend!</i>
                </div>
            </div>
            <div class="card">
                <div class="card-header sm">Meine Möglichkeiten</div>
                <div class="card-body">
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <i class="fa fa-check"></i>
                        {!! \Session::get('success') !!}
                    </div>
                    @endif
                    <h4>Ich kann / habe aktuell ...</h4>
                    <form method="post" action="{{route("user.patch")}}">
                        <input type="hidden" name="field" value="bio">
                        @method("patch")
                        @csrf
                        <?php
                            $skills = [
                                "springer"=>"Als Springer zu arbeiten (Spontan bei anderen Teams mitarbeiten)",
                                "kamera"=>"Kamera/Handy zum Filmen",
                                "licht"=>"Licht/Reflektoren/Molton",
                                "schnitt"=>"Schnittrechner + Resolve/FinalCut/Avid/Premiere",
                                "schauspiel"=>"Schauspiel, Interviewer",
                                "regie"=>"Organisieren, Recherchieren",
                                "drehbuch"=>"Recherchieren, Schreiben",
                                "drohne"=>"Drohnenschein+Drohne",
                                "sounddesign"=>"Sounddesign",
                                "soundtrack"=>"Soundtrack/Musik abmischen"
                            ];
                        ?>
                        @foreach($skills as $skill=>$title) 
                            <input type="checkbox" class="mt-1 custom-checkbox" name="skill_{{$skill}}" 
                                @if(auth()->user()->hasSkill($skill)) 
                                    checked="true"
                                @endif
                                value="true" > 
                                @if($skill === "springer")
                                <span class="text-warning"><b>NEU!!!</b> {{$title}}</span><br>
                                @else
                                    {{$title}}<br>
                                @endif
                        @endforeach
                        <textarea name="bio" class="mt-2 form-control" maxlength="300" placeholder="Stell Dich kurz vor, seit wann machst Du Film? Welche Erfahrungen und Stärken kannst Du bei diesem Projekt einbringen?">{{auth()->user()->getBioText()}}</textarea>
                        <input type="submit" class="btn btn-primary" value="Skills/Bio aktualisieren">
                    
                    </form>
                </div>
            </div>
                </div>
        <div class="col-md-3">

            @if(auth()->user()->enabled)
            <div class="card">
                <div class="card-header sm">Aufgaben</div>
                    <div class="list-group">
                        @if (!auth()->user()->hasTeam() && auth()->user()->can("teamer"))

                            <a href="{{route("teams.create")}}" class="list-group-item list-group-item-action active">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Ein Team bilden</h5>
                                    <small>Jetzt</small>
                                </div>
                                <small>Im Team macht die Arbeit immer mehr Spa&szlig; :D</small>
                            </a>

                        @elseif(auth()->user()->getBioText() == "" || count(auth()->user()->weeksInFuture()) < 1)

                            @if(count(auth()->user()->weeksInFuture()) < 1)
                            <a href="?" class="list-group-item list-group-item-action active">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Wähle Deine Wochen</h5>
                                    <small>Jetzt</small>
                                </div>
                                <small>Bitte wähle die Wochen aus, in denen Du im Camp bist/sein möchtest.</small>
                            </a>
                            @endif
                            @if(auth()->user()->getBioText() == "")
                            <a href="?" class="list-group-item list-group-item-action active">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">Stelle Dich vor!</h5>
                                    <small>Jetzt</small>
                                </div>
                                <small>Schreibe einen kleinen Text unter "Meine Möglichkeiten", damit Deine Dozenten eine bessere Übersicht haben. Danke!</small>
                            </a>
                            @endif

                        @else

                            <div style="padding:80px 20px;text-align:center;color:#ffffff33">Es gibt aktuell keine neuen Aufgaben.</div>

                        @endif
                    </div>
            </div>

            @if(auth()->user()->teams_invite_redeem_url!=null)
            <div class="card">
                <div class="card-header sm">Teams Zugang</div>
                <div class="card-body">
                    <b>Deine E-mail</b>:<br>
                    {{auth()->user()->email}}
                    <div class="mt-3"></div>
                    <b>Jetzt aktivieren</b>:<br>
                    <a href="{{auth()->user()->teams_invite_redeem_url}}" target="_blank"><i class="fa fa-external-link"></i> Hier klicken</a>
                    <div class="mt-3" style="opacity:.4;color:white;font-size:.8em;line-height:1rem;">Eure Dozenten teilen euch, sobald es soweit ist, mit, wo und wie Ihr euch mit diesen Login-Daten einloggen könnt.</div>
                </div>
            </div>
            @endif

        </div>


        @else
        <div class="card">
            <div class="card-header sm p-3" style="background:orangered !important">Inaktiver Account</div>
            <div class="p-4 text-white text-center" style="max-width:500px;margin:auto">
                Dein Account muss leider noch manuell freigeschaltet werden, das dauert in der Regel allerdings nur einige Minuten.<br><a href="?reload"><i class="fa fa-refresh"></i> Seite neuladen</a><br><br>
                <b class="text-warning"><i class="fa fa-exclamation-circle"></i> Da aktuell alle Teams voll sind, wirst Du frühestens ab <u>Sonntag Abend</u> freigeschaltet!</b><br><br>
                Bei weiteren Fragen kannst Du uns einfach kurz anrufen <a href="tel:+4915258799871">+4915258799871</a> oder Mailen.
                <br>
                <br>
                <a href="{{route("team")}}" class="btn btn-primary">Ansprechpartner</a>
                <br>
                <br>
                Liebe Grüße,<br>
                Nele, Vicky, Paul, Cyril und Dominik
            </div>
        </div>
    </div>
    @endif

</div>
</div>
</div>
@endsection
