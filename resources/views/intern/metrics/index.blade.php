
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <h1>Live Metrics</h1>
    <h6 class="text-white">
        @if(isset($user))
            <a href="{{route("metrics.live")}}"><i class="fa fa-chevron-left"></i> Alle Metrics</a> | 
            User: {{$user->name}} | Ergebnisse: {{count($metrics) >= 999 ? "999+" : count($metrics)}} 
        @elseif($by=="route")
            <a href="{{route("metrics.live")}}"><i class="fa fa-chevron-left"></i> Alle Metrics</a> | 
            Route: {{strtoupper($route)}} | Ergebnisse: {{count($metrics) >= 999 ? "999+" : count($metrics)}} 
        @else
            <i class="fa fa-home"></i> Alle Metrics | <span class="text-muted">User oder Route anklicken</span>
        @endif
    </h6>
    <table class="table table-striped table-hover">
        @forelse($metrics as $metric)
        <tr>
            <td><div class="indicator" style="background:{{$metric->getHue()}}">
                    {{$metric->user == null ? "?" : 
                        ($metric->user->is_admin ? "A" : ($metric->user->is_docent ? "D" : "T"))
                    }}
                </div></td>
            <td>{{$metric->created_at->diffForHumans()}}</td>
            <td><a href="{{$metric->user_id !== null ? route("metrics.user",$metric->user_id) : ""}}">{!!$metric->user->name ?? '<span class=text-muted>Besucher</span>'!!}</a></td>
            <td><a href="{{route("metrics.route",$metric->route)}}" style="text-transform:uppercase">{{$metric->route}}</a></td>
            <td>{{$metric->path}}</td>
            <td>{{$metric->user_agent}}</td>
        </tr>
        @empty
        Keine Live Metrics verfügbar.
        @endforelse
    </table>
</div>
@endsection
