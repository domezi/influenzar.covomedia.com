
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Team anmelden</div>
                <div class="card-body">
                    <form method="post" action="{{route("teams.store")}}">
                        @csrf
                        @error("title")
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="title" autofocus="true" value="{{old("title")}}" placeholder="Teamname, z.B. Team Berlin, Team Dresden, Lochmeister-Crew, ..." class="form-control">
                        <input type="submit" value="Team erstellen" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
