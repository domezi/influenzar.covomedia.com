
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Meine Teams</div>
                <div class="card-body">
                    @forelse($myTeams as $team)
                        <a href="{{route("teams.show",$team->id)}}" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$team->title}}</h5>
                                <small>{{$team->updated_at->diffForHumans()}}</small>
                            </div>
                            <small>{{implode(", ",$team->members->pluck('name')->toArray())}}</small>
                        </a>
                    @empty
                        Du bist aktuell noch nicht Dozent*in eines Teams.
                    @endforelse
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Andere Teams</div>
                <div class="card-body">
                    @forelse($teams as $team)
                        <a href="{{route("teams.show",$team->id)}}" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$team->title}}</h5>
                                <small>{{$team->updated_at->diffForHumans()}}</small>
                            </div>
                            <small>{{implode(", ",$team->members->pluck('name')->toArray())}}</small>
                        </a>
                    @empty
                        Es gibt aktuell keine anderen Teams.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
