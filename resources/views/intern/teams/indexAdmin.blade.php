
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <h1>Teams</h1>
    <admin-teams-table 
         team-route="{{ route('teams.show.admin','team_id') }}"
         teams-route="{{ route('teams.index.admin','team_id') }}"
         users-route="{{ route('users.index') }}"
         team-patch-route="{{ route('team.patch','team_id') }}"
         team-delete-route="{{ route('teams.delete','team_id') }}"
         :admin="{{auth()->user()}}"></admin-teams-table>
</div>
@endsection
