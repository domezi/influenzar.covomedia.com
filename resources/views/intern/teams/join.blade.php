
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Team beitreten</div>
                <div class="card-body">
                    @if (!auth()->user()->hasTeam())
                    <a href="{{route("teams.create")}}" class="list-group-item list-group-item-action active">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Ein Team bilden</h5>
                            <small>Jetzt</small>
                        </div>
                        <small>Im Team macht die Arbeit immer mehr Spass :D</small>
                    </a>
                    @else
                    <div style="padding:15px;text-align:center">
                        Gratulation! Du bist dem Team "{{auth()->user()->team->title}}" beigetreten.
                        <br>
                        <br>
                        <a href="{{route("teams.showOurTeam")}}" class="btn btn-primary">Team anzeigen</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
