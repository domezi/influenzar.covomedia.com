
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Team beitreten</div>
                <div class="card-body">
                    <div style="padding:15px;text-align:center">
                        <h4>
                            Hey, geh noch nicht!
                        </h4>
                        <br>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/OcHlYBVnlZA?autoplay=true" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <br>
                        <br>
                        <p>Du hast Dein Team verlassen.</p>
                        <br>
                        <a href="{{route("home")}}" class="btn btn-primary">Startseite</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
