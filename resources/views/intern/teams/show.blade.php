
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Team "{{$team->title}}"</div>
                <div class="card-body">
                   
                        <h2>Einladen</h2>
                        Um andere Teilnehmer*innen in das Team "{{$team->title}}" einzuladen, bitte die Teilnehmer*innen, diesen Link zu verschicken:
                        <br>
                        <kbd style="font-family:mono">
                            {{route("teams.join",$team->id)}}
                        </kbd>
                        <hr>

                        <h2>Dozent*in</h2>
                        @can("admin")
                            @if($team->docent != null)
                                <div style="font-size:1.0em">{{$team->docent->name}} </div>
                            @else
                                Dieses Team hat aktuell keine*n betreuende*n Dozenten*in. <br>
                            @endif
                        @else
                            @if($team->docent != null)
                                <div style="font-size:1.0em">{{$team->docent->name}} </div>
                                <i class="fa fa-envelope"></i> <a href="mailto:{{$team->docent->email}}">{{$team->docent->email}}</a>
                                @if($team->docent->mobile_number)
                                    <br> <i class="fa fa-phone"></i> <a href="mailto:{{$team->docent->mobile_number}}">{{$team->docent->mobile_number}}</a>
                                @endif
                                <div style="height:10px"></div>
                                @if($team->docent->id == auth()->id())
                                    <a href="{{route("teams.freeDocent",$team->id)}}" class="btn btn-primary">Team verlassen</a>
                                @else
                                    <a href="{{route("teams.becomeDocent",$team->id)}}" class="btn btn-primary">Dozenten durch mich ersetzen</a>
                                @endif
                            @else
                                Dieses Team hat aktuell keine*n betreuende*n Dozenten*in. <br>
                                Hättest Du Lust?
                                <div style="height:10px"></div>
                                <a href="{{route("teams.becomeDocent",$team->id)}}" class="btn btn-primary">Ja, Dozent werden</a>
                            @endif
                        @endcan


                        <hr>
                        <h2>Themen</h2>
                            @can("admin")
                        <div style="white-space:pre-line">{{$team->thema}}</div>
                        @else
                        <thema-bearbeiten 
                           thema="{{$team->thema}}" 
                           route="{{route("teams.update",$team->id)}}"
                           >
                            <form method="post" action="{{route("teams.update",$team->id)}}">
                                @csrf
                                @method("patch")
                                @error("thema")
                                    <div class="text-danger">{{$message}}</div>
                                @enderror
                                <textarea name="thema" class="form-control">{{old('thema',$team->thema)}}</textarea>
                                <input type="submit" class="btn btn-primary" value="Thema speichern">
                            </form>
                        </thema-bearbeiten>
                        @endcan
                        <hr>

                        <h2>Themen Wünsche (3 Stück)</h2>
                        <div style="white-space:pre-line">{{$team->thema_wunsch}}</div>

                        <hr>
                        <h2>Uploads</h2>
                            <a href="{{route("uploads.index",$team->id)}}" class="btn btn-primary">Alle Uploads von diesem Team</a>
                        <hr>
                        <h2>Aufgaben</h2>
                        Derzeit können keine Aufgaben von Dir erstellt werden.

                        <hr>
                        <h2>Mitglieder</h2>
                        @foreach($team->members as $member) 
                            @can("admin")
                            <span class="list-group-item list-group-item-action active">
                            @else
                            <a href="{{route("users.show",$member->id)}}" class="list-group-item list-group-item-action active">
                            @endcan
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{$member->name}}</h5>
                                    <small>{{$member->updated_at->diffForHumans()}}</small>
                                </div>

                                @if($team->created_by == $member->id)
                                    <small>Ansprechpartner der Gruppe</small>
                                @else
                                    <small>Teilnehmer</small>
                                @endif

                            @can("admin")
                            </span>
                            @else
                            </a>
                            @endcan

                        @endforeach


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
