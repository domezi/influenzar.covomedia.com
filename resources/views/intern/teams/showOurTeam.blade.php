
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dein Team</div>
                <div class="card-body">
                    @if (!auth()->user()->hasTeam())
                    <a href="{{route("teams.create")}}" class="list-group-item list-group-item-action active">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Ein Team bilden</h5>
                            <small>Jetzt</small>
                        </div>
                        <small>Im Team macht die Arbeit immer mehr Spa&szlig; :D</small>
                    </a>
                    @else

                        <h2>Einladen</h2>
                        Du bist Teil des Teams "{{auth()->user()->team->title}}". Um andere in euer Team einzuladen, verschickt diesen Link:
                        <br>
                        <kbd style="font-family:mono">
                            {{route("teams.join",auth()->user()->team_id)}}
                        </kbd>

                        <hr>
                        <h2>Dozent*in</h2>
                        @if($team->docent != null)
                            <div style="font-size:1.0em">{{$team->docent->name}} </div>
                            <i class="fa fa-envelope"></i> <a href="mailto:{{$team->docent->email}}">{{$team->docent->email}}</a>
                                @if($team->docent->mobile_number)
                                    <br> <i class="fa fa-phone"></i> <a href="mailto:{{$team->docent->mobile_number}}">{{$team->docent->mobile_number}}</a>
                                @endif
                        @else
                            Aktuell habt ihr noch keine*n betreuende*n Dozenten*in. <br>
                            Aber keine Sorge, wir melden uns bei euch :D
                        @endif

                        <hr>
                        <h2>Themen</h2>
                        <div style="white-space:pre-line">{{$team->thema}}</div>
                        <hr>

                        <thema-wunsch-bearbeiten 
                           thema="{{$team->thema_wunsch}}" 
                           route="{{route("teams.thema_wunsch.update",$team->id)}}"
                           >
                            <form method="post" action="{{route("teams.thema_wunsch.update",$team->id)}}">
                                @csrf
                                @method("patch")
                                @error("thema_wunsch")
                                    <div class="text-danger">{{$message}}</div>
                                @enderror
                                <textarea name="thema_wunsch" class="form-control">{{old('thema_wunsch',$team->thema_wunsch)}}</textarea>
                                <input type="submit" class="btn btn-primary" value="Thema_wunsch speichern">
                            </form>
                        </thema-wunsch-bearbeiten>



                        <hr>
                        <h2>Aufgaben</h2>
                        Es gibt aktuell keine neuen Aufgaben.

                        <hr>
                        <h2>Mitglieder</h2>
                        @foreach($team->members as $member) 
                            <div class="list-group-item list-group-item-action active">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{$member->name}}</h5>
                                    <small>{{$member->updated_at->diffForHumans()}}</small>
                                </div>

                                @if($team->created_by == $member->id)
                                    <small>Ansprechpartner der Gruppe</small>
                                @else
                                    <small>Teilnehmer</small>
                                @endif

                            </div>
                        @endforeach


                        <hr>
                        <h2>Team verlassen</h2>
                        <p>Hier kannst Du das Team verlassen <a href="{{route("team.leave")}}" class="text-warning"><i class="fa fa-external-link"></i> Team für diese Woche verlassen</a>.</p>


                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
