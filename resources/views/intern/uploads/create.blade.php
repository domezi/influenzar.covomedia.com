
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                @if(auth()->user()->team_id==$team->id)
                    <div class="card-header">Upload hinzufügen</div>
                @else
                    <div class="card-header">Uploads für {{$team->title}} hinzufügen</div>
                @endif

                <div class="card-body">
                    <p class="text-warning"><u>Änderung:</u> Wir sind von Vimeo zu Google Drive gewechselt. Bitte ladet euer Video in die Google Drive (NICHT MEHR VIMEO), und fügt den Link in dieser Seite ein.</p>
                    <form method="post" action="{{route("uploads.store",$team->id)}}">
                        @csrf

                        @error("title")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="title" autofocus="true" value="{{old("title")}}" placeholder="Titel des Videos" class="form-control">
                 
                        @error("location")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="location" autofocus="true" value="{{old("location")}}" placeholder="Orte des Geschehens, durch Kommata getrennt (z.B: Madrid,Spanien,Paris,Arendsee)" class="form-control">
                 
                        @error("url_final")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="url_final" type="url" autofocus="true" value="{{old("url_final")}}" placeholder="URL zu GOOGLE DRIVE (Mit Musik und Grading)" class="form-control">

                        @error("url_raw")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="url_raw" type="url" autofocus="true" value="{{old("url_raw")}}" placeholder="URL zu GOOGLE DRIVE (Nur O-Ton, Keine Musik und kein Grading)" class="form-control">
                 
                        @error("genre")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="genre" autofocus="true" value="{{old("genre")}}" placeholder="Genre des Videos (z.B.: Interview, Spielfilm, Reportage, Animiert, ...)" class="form-control">
                 
                        @error("tags")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <textarea name="tags" autofocus="true" placeholder="Mehrere Stichworte für den Upload durch Kommata getrennt, z.B: interview,wasser,krankenhäuser,betroffene,alte person,natur,lustig,drohne,..." class="form-control">{{old("tags")}}</textarea>

                        <input type="submit" value="Video hinzufügen" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
