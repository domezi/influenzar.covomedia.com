
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                @if(auth()->user()->team_id==$team->id)
                    <div class="card-header">Schnittbilder hinzufügen</div>
                @else
                    <div class="card-header">Schnittbilder für {{$team->title}} hinzufügen</div>
                @endif

                <div class="card-body">
                    <form method="post" action="{{route("uploads.store",$team->id)}}">
                        @csrf

                        @error("title")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="title" autofocus="true" value="{{old("title")}}" placeholder="Titel des Schnittbilder Sammlung" class="form-control">
                 
                        @error("location")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="location" autofocus="true" value="{{old("location")}}" placeholder="Orte des Geschehens, durch Kommata getrennt (z.B: Madrid,Spanien,Paris,Arendsee)" class="form-control">

                        @error("url_raw")
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="url_raw" type="url" autofocus="true" value="{{old("url_raw")}}" placeholder="URL zu Google Drive, OneDrive, Dropbox, Mega (Nur O-Ton, Keine Musik und kein Grading)" class="form-control">

                        @error("genre")
                        <div class="text-danger">{{$message}}</div>
                        @enderror
                        <input name="genre" type="hidden" autofocus="true" value="Schnittbilder" class="form-control">
                 
                        @error("tags")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <textarea name="tags" autofocus="true" placeholder="Mehrere Stichworte für den Upload durch Kommata getrennt, z.B: interviews,wasser,krankenhäuser,betroffene,alte person,natur,lustig,drohne,..." class="form-control">{{old("tags")}}</textarea>

                        <input type="submit" value="Schnittbilder hinzufügen" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
