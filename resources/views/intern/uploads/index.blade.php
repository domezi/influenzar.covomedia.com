
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                @if(auth()->user()->team_id==$team->id)
                    <div class="card-header">Unsere Uploads</div>
                @else
                    <div class="card-header">Uploads von {{$team->title}}</div>
                @endif

                <div class="card-body">
                    <div style="display:flex">
                        <a style="flex:1" href="{{route("uploads.create",$team->id)}}" class="d-block mb-3 btn btn-primary"><i class="fa fa-plus"></i> Video </a>
                        <a style="flex:1" href="{{route("uploads.createStock",$team->id)}}" class="d-block mb-3 btn btn-primary"><i class="fa fa-plus"></i> Schnittbilder </a>
                        <a style="flex:1;margin-right:0" href="{{route("uploads.createBTS",$team->id)}}" class="d-block mb-3 btn btn-primary"><i class="fa fa-plus"></i> Making of </a>
                    </div>
                    @forelse($uploads as $upload)
                        <a href="{{route("uploads.show",$upload->id)}}" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">{{$upload->title}}</h5>
                                <small class="text-right">
                                    {!!
                                    str_replace("publish","<span class='text-warning'>Zur Sichtung freigegeben</span>",
                                        str_replace("publish_go","<span class='text-primary'>Öffentlich</span>",
                                            str_replace("archive","<span class='text-danger'>Gelöscht/Archiviert</span>",
                                                    str_replace("draft","Entwurf",
                                                        $upload->status
                                                    )
                                                )
                                            )
                                        )
                                    !!}
                                    <br>
                                    {{$upload->updated_at->diffForHumans()}}
                                </small>
                            </div>
                            <small>Hochgeladen von {{$upload->uploader->name}}</small>
                        </a>
                    @empty
                        Es gibt noch keine Uploads hier.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
