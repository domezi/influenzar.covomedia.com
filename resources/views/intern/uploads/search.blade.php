
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Uploads finden</div>

                <div class="card-body">

                    <form method="post" action="{{route("uploads.search")}}">
                        @csrf
                        <input name="search" placeholder="Suchbegriffe, Tags, Genre eingeben ..." value="{{old("search",$search)}}" class="form-control">
                    
                    </form>


                    @forelse($uploads as $upload)
                        @if($upload->status != "archive")
                        <a target="_blank" href="{{route("uploads.show",$upload->id)}}" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1"><i class="fa fa-external-link"></i> {{$upload->title}}</h5>
                                <small class="text-right">
                                    {!!
                                    str_replace("publish","<span class='text-warning'>Zur Sichtung freigegeben</span>",
                                        str_replace("publish_go","<span class='text-primary'>Öffentlich</span>",
                                            str_replace("archive","<span class='text-danger'>Gelöscht/Archiviert</span>",
                                                    str_replace("draft","Entwurf",
                                                        $upload->status
                                                    )
                                                )
                                            )
                                        )
                                    !!}  
                                    <br>
                                    {{$upload->updated_at->diffForHumans()}}
                                </small>

                            </div>
                            <small>Hochgeladen von {{$upload->uploader->name}}</small>
                        </a>
                        @endif
                    @empty
                        Hier gibt es aktuell noch keine Uploads.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
