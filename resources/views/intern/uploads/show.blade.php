
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Details zum Upload</div>
                <div class="card-body">

                    @if($upload->team->id == Auth::user()->team_id || Gate::allows("admin") || Gate::allows("docent"))

                        <h3>Titel: {{$upload->title}}</h3>
                        <hr>
                        <h5>Genre: {{$upload->genre}}</h5>
                        <h5>Hochgeladen von: {{$upload->team->title}}, {{$upload->uploader->name}}</h5>
                        <h5>Thema: {{$upload->team->thema}}</h5>
                        @if($upload->team->docent)
                            <h5>Dozent: {{$upload->team->docent->name}}</h5>
                        @endif

                        <h5>Status: 
                            @canany(["docent","admin"])
                            <br>
                            <form class="mt-2 d-inline-block" method="post" action="{{route('uploads.patch',$upload->id)}}">
                                @method("patch")
                                @csrf
                                <select onchange="this.form.submit()" name="status" class="form-control d-inline-block" style="width:210px">

                                    <option 
                                    @if($upload->status == "draft")
                                    selected
                                    @endif
                                    value="draft">Entwurf</option>

                                    <option
                                    @if($upload->status == "archive")
                                    selected
                                    @endif
                                    value="archive">Archiv/Ausgeblendet</option>

                                    @can("admin")
                                        <option
                                        @if($upload->status == "publish")
                                        selected
                                        @endif
                                        value="publish">Zur Sichtung freigegeben</option>
                                        <option
                                        @if($upload->status == "publish_go")
                                        selected
                                        @endif
                                        value="publish_go">Öffentlich</option>
                                    @endcan

                                </select>
                            </form>
                            @else
                                {!!
                                str_replace("publish","<span class='text-warning'>Zur Sichtung freigegeben</span>",
                                    str_replace("publish_go","<span class='text-primary'>Öffentlich</span>",
                                        str_replace("archive","<span class='text-danger'>Gelöscht/Archiviert</span>",
                                                str_replace("draft","Entwurf",
                                                    $upload->status
                                                )
                                            )
                                        )
                                    )
                                !!}
                                @if(auth()->id() == $upload->uploaded_by && $upload->status == 'draft') 
                                    <form class="d-inline-block" method="post" action="{{route('uploads.patch',$upload->id)}}">
                                        @method("patch")
                                        @csrf
                                        <input type="hidden" name="status" value="archive">
                                        <button type="submit" value="" class="btn btn-primary btn-primary-danger btn-sm"><i class="fa fa-trash"></i> Upload archivieren </button>
                                    </form>
                                @endif
                            @endcan
                        </h5>
                         
                        <h5>Beschreibung</h5>
                            @canany(["admin"])
                            <form class="d-inline-block" method="post" style="width:100%" action="{{route('uploads.patch',$upload->id)}}">
                                @csrf
                                @method("patch")
                                <textarea class="form-control d-block" name="description" placeholder="Eine kurze Videobeschreibung">{{$upload->description}}</textarea>
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Beschreibung speichern </button>
                            </form>
                            @endcan
                        <hr>

                        <h5>Credits</h5>
                            <form class="d-inline-block" method="post" style="width:100%" action="{{route('uploads.patch',$upload->id)}}">
                                @csrf
                                @method("patch")
                                <textarea class="form-control d-block" name="credits" placeholder="Credits/Teamaufgaben/Abspann hinzufügen">{{$upload->credits}}</textarea>
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Credits speichern </button>
                            </form>
                        <hr>
                        Hier die Links:

                        @if($upload->url_final != "")
                        <a href="{{$upload->url_final}}" target="_blank" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1"><i class="fa fa-film"></i> Video, mit Musik und Grading</h5>
                                <small>{{$upload->updated_at->diffForHumans()}}</small>
                            </div>
                            <small>{{$upload->url_final}}</small>
                        </a>
                        @endif

                        <a href="{{$upload->url_raw}}" target="_blank" class="mt-1 list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                @if($upload->genre == "Schnittbilder")
                                <h5 class="mb-1"><i class="fa fa-download"></i> Drive Link mit allen Schnittbildern</h5>
                                @else
                                <h5 class="mb-1"><i class="fa fa-film"></i> Video, nur O-Ton, keine Musik und kein Grading</h5>
                                @endif
                                <small>{{$upload->updated_at->diffForHumans()}}</small>
                            </div>
                            <small>{{$upload->url_raw}}</small>
                        </a>

                        <hr>
                        <div class="tags mt-3">
                            Tags:
                            <?php $type = "success"; ?>
                            @foreach(array_unique(explode(",",$upload->tags)) as $tag) 
                            <?php if($tag == "||") {$type = "primary";continue;}?>
                            <a href="{{route("uploads.byTag",$tag)}}" class="badge badge-pill badge-{{$type}}">{{$tag}}</a>
                            @endforeach
                        </div>

                    @else
                    <i class="fa fa-lock" style="font-size:5em;margin:40px auto;text-align:center;display:block;"></i>
                    <i style="text-align:center;margin-bottom:50px;display:block;">Du darfst diesen Upload aktuell nicht einsehen.</i>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
