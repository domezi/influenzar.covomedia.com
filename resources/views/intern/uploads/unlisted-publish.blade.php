
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Uploads zur Sichtung</div>
                <div class="card-body">
                    <table class="table table-striped">
                        @forelse($uploads as $upload) 
                        <tr>
                            <td>{{$upload->title}}</td>
                            <td>{{$upload->description}}</td>
                            <td>{{$upload->updated_at->diffForHumans()}}</td>
                            <td><a href="{{$upload->url_final}}" target="_blank"><i class="fa fa-external-link"></i> Video anschauen</a></td>
                        </tr>
                        @empty
                        <p>Aktuell keine Uploads zur Sichtung. </p>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
