
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <h1>User</h1>
    <admin-users-table 
         user-patch-route="{{ route('users.patch','user_id') }}"
         teams-route="{{ route('teams.index.admin') }}"
         user-delete-route="{{ route('users.delete','user_id') }}"
         team-route="{{ route('teams.show.admin','user_id') }}"
         :admin="{{auth()->user()}}"></admin-users-table>
</div>
@endsection
