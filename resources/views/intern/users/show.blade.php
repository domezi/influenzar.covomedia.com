
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$user->name}}</div>
                <div class="card-body" style="font-size:1.2em;text-align:center;line-height:1.4em;">
                    <i class="fa fa-users"></i> <a href="{{route("teams.show",$user->team->id)}}">{{$user->team->title}}</a><br>
                    <i class="fa fa-envelope"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                    @if($user->mobile_number)
                    <br> <i class="fa fa-phone"></i> <a href="mailto:{{$user->mobile_number}}">{{$user->mobile_number}}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
