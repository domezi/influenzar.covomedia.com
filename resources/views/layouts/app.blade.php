<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Filmemachen in der Praxis lernen - in einer Woche von der Idee zum fertigen Film - in kleinen Teams unter Anleitung erfahrener Dozenten - in Arendsee im Herzen Sachsen-Anhalts, mitten in der wunderbaren Altmark - kreativ sein und neue Perspektiven entdecken!"/>
    <meta name="keywords" content="Jugendfilmcamp, Jugend, Film, Camp, Sommerferien, Filmcamp, Feriencamp, Kamera, Film, Movies, Movie, Kino, Schauspiel, Schauspieler, Workshop, Arendsee, CGI, VFX, DSLR, Baden, Ferien, Sachsen-Anhalt, Altmark, Spass, Stunt, Maskenbild, Photography, Fotografie, Dokumentarfilm, VR, Klassenfahrt, Exkursion"/>
    <meta name="author" content="Dominik Ziegenhagel" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <meta name="theme-color" content="#f0ab3f" />

    <link rel="icon" type="image/png" href="https://jugendfilmcamp.de/favicons/favicon-96x96.png" sizes="96x96" />
	  
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- og -->
    <!--opengraph für Facebook sharing -->
    <meta property="og:url" content="https://www.jugendfilmcamp.online" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Influenzar 2020 Jugendfilmcamp" />
    <meta property="og:description" content="Die ganze Welt ist in Aufruhr.  Covid-19, verursacht durch das neuartige Corona Virus, stürzt die Menschheit in eine tiefe Krise.  Scheinbar unaufhaltsam.  Wir als Jugendfilmcamper*innen MÜSSEN reagieren: So, wie wir sind… Jung. Kreativ. Kritisch. Aufwühlend. Visionär.  Lasst uns das machen, was uns brennt… filmische Zeitdokumente der jungen Generation zur Krise.        " />
    <meta property="og:image" content="https://www.jugendfilmcamp.online/img/jfc-facebook.jpg" />
    <meta property="og:image:url" content="https://www.jugendfilmcamp.online/img/jfc-facebook.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="1500" />
    <meta property="og:image:height" content="786" />

    <!-- Scripts -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Styles -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163570708-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
<?php if(auth()->id()>0) {?>
      gtag('set', {'user_id': <?php echo auth()->id();?>}); 
    <?php } ?>
      gtag('config', 'UA-163570708-1');
    </script>
    <script>
    function showVideo(id) {
        // show overlay
        var div = document.getElementById("fullscreen-video");
        div.style.display = "block";

        // make loading
        var iframewrapper = document.getElementById("video-close-wrapper");
        var iframeloader = document.getElementById("wrap-spinner");
        iframewrapper.style.display = "none";
        iframeloader.style.display = "block";

        // load video
        var iframe = document.getElementById("fullscreen-video-iframe");
        iframe.src= "https://www.youtube.com/embed/"+id+"?autoplay=true";
        setTimeout(function() {
            iframewrapper.style.display = "block";
            iframeloader.style.display = "none";
        },200);
    }

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var isEscape = false;
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc");
        } else {
            isEscape = (evt.keyCode === 27);
        }
        if (isEscape) {
            hideVideo();
        }
    };
    function hideVideo() {
        // clean up
        var iframe = document.getElementById("fullscreen-video-iframe");
        iframe.src= "";
        var iframewrapper = document.getElementById("video-close-wrapper");
        var iframeloader = document.getElementById("wrap-spinner");
            iframewrapper.style.display = "block";
            iframeloader.style.display = "none";

        var div = document.getElementById("fullscreen-video");
        div.style.display = "none";
    }
    </script>

    <style>
        body {background-color:#03070D !important}

        @if(substr(\Route::currentRouteName(),0,7) == "metrics" || \Route::is("users.index") || \Route::is("teams.index.admin"))
        body {background-color:#4f5968 !important; background-image:none !important}
        @elseif(\Route::is("register"))
        body {background-image:url({{asset("img/home/bg2.jpg")}})}
        @else
        body {background-image:url({{asset("img/home/bg.jpg")}})}
        @endif

        .navbar-dark .nav-item .nav-link {color:white}
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-static navbar-dark bg-dark shadow-sm">
            <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset("/img/jfc-logo-flach.png")}}" height="30" alt="{{ config('app.name', 'Laravel') }}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('welcome') }}">Start</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('faq') }}">Häufige Fragen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('team') }}">Ansprechpartner</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Start</a>
                            </li>

                            @can("docent")
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('uploads.search') }}">Alle Uploads</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('teams.index') }}">Meine Teams</a>
                            </li>
                            @endcan

                            @can("admin")
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('users.index') }}"><i class="fa fa-shield"></i> User-P.</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('teams.index.admin') }}"><i class="fa fa-shield"></i> Team-P.</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('videos.index') }}"><i class="fa fa-shield"></i> Öffentliche Videos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('uploads.search') }}"><i class="fa fa-shield"></i> Uploads</a>
                            </li>
                            @endcan

                            @can("teamer")
                            @if(auth()->user()->enabled)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('teams.showOurTeam') }}">Team</a>
                            </li>
                            @endif
                            @if(auth()->user()->hasTeam())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('uploads.index',auth()->user()->team->id) }}">Uploads</a>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('team') }}">Ansprechpartner</a>
                            </li>
                            @endcan

                            @canany(["docent","teamer"])
                            @if(auth()->user()->enabled)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('links') }}">Links</a>
                            </li>
                            @endif
                            @endcanany

                            <li class="nav-item">
                                <a class="nav-link text-success" href="{{ route('reportbug') }}"><i class="fa fa-bug"></i> Fehler melden</a>
                            </li>

                        @endguest
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main style="margin-top:85px;">
            @yield('content')
        </main>



        <!--footer-->
        <div class="bottom-box">
            <!-- START ROW -->
            <div class="container">
            <div class="row gutters">
                <div class="col-md-7">
                    <div class="contact-details">
                        <!-- START DETAIL -->
                        <span class="detail">
                            <span class="text">Das Jugendfilmcamp ist ein Projekt der gemeinnützigen <a href="http://www.youvista.de">YouVista UG</a>. © YouVista UG (haftungsbeschränkt).</span>
                        </span><!-- END DETAIL -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="contact-details">
                        <!-- START DETAIL -->
                        <span class="detail">
                            <span class="text">Design by Giovanni Zeitz</span><br>
                            <span class="text">Coding by <a href="https://www.ziegenhagel.com" target="_blank">Dominik Ziegenhagel</a></span>
                        </span><!-- END DETAIL -->
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="contact-details">
                        <!-- START DETAIL -->
                        <span class="detail">
                            <span class="text"><a href="https://www.jugendfilmcamp.de/impressum.html">Impressum </a></span>
                            <span class="text">&nbsp;⁄&nbsp;</span>
                            <span class="text"><a href="https://www.jugendfilmcamp.de/agb.html"> AGBs</a></span>
                            <span class="text">&nbsp;⁄&nbsp;</span>
                            <span class="text"><a href="https://www.jugendfilmcamp.de/datenschutzerklaerung.html"> Datenschutzerklärung</a></span>
                        </span><!-- END DETAIL -->
                    </div>
                </div>
            </div><!-- END ROW -->
        </div>
</div>



    </div>
</body>
</html>
