@extends('layouts.app')

@section('content')
<div class="container def">
        <h1 style="">
            Wichtige Links
        </h1>
     
        <div class="assets" style="color:white; white-space:pre-line">
            <div class="link mb-4 mt-4">
                <h2>Einverständniserklärung</h2>
                <p>Achtung! Immer unterzeichnen lassen, falls ihr externe Personen filmt bzw. interviewt.</p>
                <a target="_blank" href="{{asset('files/Einverständniserklärung Influenzar 2020.pdf')}}" class="btn btn-primary"><i class="fa fa-download"></i> Jetzt downloaden</a>
            </div>
            <div class="link mb-4 mt-4">
                <h2>Filmauftrag/gesetzliche Regelungen/Bestimmungen beim Drehen</h2>
                <p>Achtung! Immer bei euch tragen.</p>
                <a target="_blank" href="{{asset('files/Filmauftrag_gesetzliche Regelungen_Hygienetipps beim Drehen influenzar 2020.pdf')}}" class="btn btn-primary"><i class="fa fa-download"></i> Jetzt downloaden</a>
            </div>
            <div class="link mb-4 mt-4">
                <h2>Abspann mit Logos Jugendfilmcamp und FES</h2>
                <a target="_blank" href="{{route("assets")}}" class="btn btn-primary"><i class="fa fa-external-link"></i> Jetzt Intro erstellen</a>
            </div>
            <div class="link mb-4 mt-4">
                <h2>Links für Musik und Sound Effekte</h2>
                <a target="_blank" href="https://www.bensound.com/" class="btn btn-primary"><i class="fa fa-external-link"></i> Zu Bensound</a>
            </div>
            <div class="link mb-4 mt-4">
                <h2>Links für Filmmaterial / Stock Footage</h2>
                <a target="_blank" href="https://archive.org/details/stock_footage" class="btn btn-primary"><i class="fa fa-external-link"></i> Zum Stock Footage</a>
            </div>
        </div>

</div>
@endsection
