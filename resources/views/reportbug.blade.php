@extends('layouts.app')

@section('content')
<div class="container">
    <div style="padding:20px 30px;" class="card">
        <div class="text-center">
            <h1 style="color:white;margin-top:25px">Feedback / Fehler melden</h1>
            <img src="https://jugendfilmcamp.de/images/layout/underline.svg" width=600>
            <p class="mt-3">Vielen Dank für Dein Feedback! Bitte gib uns einige Details, damit wir den Fehler lösen können.</p>
            <form style="max-width:600px;margin:40px auto" method="post">
                @csrf

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <i class="fa fa-check"></i>
                        {!! \Session::get('success') !!}
                    </div>
                @else

                    @error("title")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <input value="" name="title" placeholder="Titel der Fehlerbeschreibung / Feature-Vorschlag" class="form-control" required>

                    @error("description")
                        <div class="text-danger">{{$message}}</div>
                    @enderror
                    <textarea name="description" placeholder="Einige Details und genauere Erklärungen, damit wir das Problem nachvollziehen können." class="form-control" required
                    ></textarea>

                    <input type="submit" value="Fehlerbericht / Feature-Vorschlag senden" class="btn btn-primary btn-block">
                @endif

            </form>
        </div>
        

    </div>
</div>
@endsection
