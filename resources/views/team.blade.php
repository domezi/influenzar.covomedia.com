@extends('layouts.app')

@section('content')
<div class="container def">

        <div class="teams">
            <h2 style="margin-bottom:5px !important;">
                JUGEND Ansprechpartner*innen
            </h2>
            <p style="margin-top:15px;text-align:center;margin-bottom:35px;opacity:.7">
            Für konkrete Fragen und kleinere Anliegen könnt ihr uns einfach kurz ne Instagram-Nachricht schreiben, egal ob Schnittfragen, Team-Orga oder Tipps.
            </p>
            <div class="row team-wrapper" style="margin-top:30px;">
                @if(false)
                <div class="col-md-4">
                    <img src="{{asset("img/team/nele.jpeg")}}">
                    <h3>
                        Nele <span style="opacity:.5">+ Helena</span>
                    </h3>
                    Teamfindung,<br>allgemeine Jugend-Orga<br>
                    @auth
               
                    <a href="mailto:n.tue@web.de"> <i class="fa fa-envelope"></i> n.tue@web.de </a><br>
                    <a href="tel:+49 175 7449983"> <i class="fa fa-phone"></i> +49 175 7449983</a><br>
                  
                    @else
                    <i>Email/Handy ist nur für Teams sichtbar.</i><br>
                    @endauth
                    <a href="https://www.instagram.com/tunefish99/"> <i class="fa fa-instagram"></i> @tunefish99 </a><br>
                </div>
                @else
                <div class="col-md-4">
                    <img src="{{asset("img/team/helena.jpeg")}}">
                    <h3>
                        Helena <span style="opacity:.5">+ Nele</span>
                    </h3>
                    Teamfindung,<br>allgemeine Jugend-Orga<br>
                    @auth
                    <a href="tel:+49 176 97392751"> <i class="fa fa-phone"></i> +49 176 97392751</a><br>
                    @else
                    <i>Handy ist nur für Teams sichtbar.</i><br>
                    @endauth
                    <a href="mailto:helena.koeppen@web.de"> <i class="fa fa-envelope"></i> helena.koeppen@web.de </a><br>
                </div>
                 @endif

                <div class="col-md-4">
                    <img src="{{asset("img/team/vicky.jpeg")}}">
                    <h3>
                        Vicky
                    </h3>
                    Kamera, Licht, Schauspiel<br>
                    wie beginne ich ...?<br>
                    @auth
                    
                    <a href="mailto:v.schueth@web.de"> <i class="fa fa-envelope"></i> v.schueth@web.de </a><br>
                    <a href="tel:+49 1575 5974705"> <i class="fa fa-phone"></i> +49 1575 5974705</a><br>

                    @else
                    <i>Email/Handy ist nur für Teams sichtbar.</i><br>
                    @endauth
                    <a href="https://www.instagram.com/diggi_diggy/"> <i class="fa fa-instagram"></i> @diggi_diggy </a><br>
                </div>
                <div class="col-md-4">
                    <img src="{{asset("img/team/paul.jpeg")}}">
                    <h3>
                        Paul
                    </h3>
                    Fragen zum Bereich Schnitt<br>
                    wie kann ich ...?<br>
                    @auth
                   
                    <a href="mailto:paulweissfilms@gmail.com"  > <i class="fa fa-envelope"></i> paulweissfilms@gmail.com </a><br>
                    <a href="tel:+49 1573 3175233"> <i class="fa fa-phone"></i> +49 1573 3175233</a><br>

                    @else
                    <i>Email/Handy ist nur für Teams sichtbar.</i><br>
                    @endauth
                    <a href="https://www.instagram.com/paulweissfilms/"> <i class="fa fa-instagram"></i> @paulweissfilms </a><br>
                </div>
            </div>
            <div class="row team-wrapper" style="margin-top:40px;">
                <div class="col-md-4">
                    <img src="{{asset("img/team/dominik.jpeg")}}">
                    <h3>
                        Dominik
                    </h3>
                    Kamera, Licht,<br> Techniklösungen :D<br>

                    <script type="text/javascript" language="javascript">
                    <!--
                    // Email obfuscator script 2.1 by Tim Williams, University of Arizona
                    // Random encryption key feature coded by Andrew Moulden
                    // This code is freeware provided these four comment lines remain intact
                    // A wizard to generate this code is at http://www.jottings.com/obfuscator/
                    { coded = "9omf@E9MpMoIgpMH.Rfz"
                    key = "uCP9hxW6tNpeELfQ1HOFyZV5I740cTJMaSmknBXqA8w2GiK3RUYbgvzsorjlDd"
                    shift=coded.length
                    link=""
                    for (i=0; i<coded.length; i++) {
                        if (key.indexOf(coded.charAt(i))==-1) {
                            ltr = coded.charAt(i)
                                link += (ltr)
                        }
                        else {     
                            ltr = (key.indexOf(coded.charAt(i))-shift+key.length) % key.length
                                link += (key.charAt(ltr))
                        }
                    }
                    document.write("<a href='mailto:"+link+"'><i class='fa fa-envelope'></i> info@z<span>ie</span>genha<!--@gmail.com-->gel.com</a>")
                    }
                    //-->
                    </script><noscript>Sorry, you need Javascript on to email me.</noscript><br>
                    <a href="tel:015258799871"> <i class="fa fa-phone"></i> +49 1525 8799871</a><br>
                    <a href="https://www.instagram.com/dommezi/"> <i class="fa fa-instagram"></i> @dommezi </a><br>
                </div>

                <div class="col-md-4">
                    <img src="{{asset("img/team/cyril.jpeg")}}">
                    <h3>
                        Cyril
                    </h3>
                    Setdesign, kreativ Tipps,<br>
                    wie realisiere ich ...?<br>
                    @auth
                    <a href="mailto:cyril.fischer@yahoo.com"> <i class="fa fa-envelope"></i> cyril.fischer@yahoo.com </a><br>
                    <a href="tel:+41 78 885 15 20"> <i class="fa fa-phone"></i> +41 78<span style="display:none">439</span> 885 15 20</a><br>
                    @else
                    <i>Email/Handy ist nur für Teams sichtbar.</i><br>
                    @endauth
                    <a href="https://www.instagram.com/cyril__1/"> <i class="fa fa-instagram"></i> @cyril__1 </a><br>
                </div>
                           </div>
            <h2 class="topline" style="margin-bottom:5px !important;">
                ANSPRECHPARTNER FÜR EURE FILMTEAMS
            </h2>
            <p style="margin-top:15px;text-align:center;margin-bottom:35px;opacity:.7">
            Jedes Team bekommt einen Ansprechpartner vom Jugendfilmcamp zu eurer Seite. Wochenweise werden die Ansprechpartner gewechselt.
            </p>
            <div class="row team-wrapper">
                <div class="col-md-4">
                    <img src="{{asset("img/team/poppe.jpg")}}">
                    <h3>
                        Matthias Poppe
                    </h3>
                    @auth
                    <a href="mailto:m.poppe@jugendfilmcamp.de">
                        m.poppe@jugendfilmcamp.de
                    </a>
                    @else
                    <i>Email ist nur für Teams sichtbar.</i>
                    @endauth
                </div>
                <div class="col-md-4">
                    <img src="{{asset("img/team/heinz.jpg")}}">
                    <h3>
                        Heinz Neumann
                    </h3>
                    @auth
                    <a href="mailto:heinzneumann3@gmx.net">
                        heinzneumann3@gmx.net
                    </a>
                    @else
                    <i>Email ist nur für Teams sichtbar.</i>
                    @endauth
                </div>
                <div class="col-md-4">
                    <img src="{{asset("img/team/fitz.jpg")}}">
                    <h3>
                        Fitz van Thom
                    </h3>
                    @auth
                    <a href="mailto:fitz@phantomproduktion.de">
                        fitz@phantomproduktion.de
                    </a>
                    @else
                    <i>Email ist nur für Teams sichtbar.</i>
                    @endauth
                </div>
            </div>
            <h2 class="topline">
                PROJEKTLEITUNG
            </h2>
            <div class="row team-wrapper">
                <div class="col-md-4">
                    <img src="{{asset("img/team/anja.jpg")}}">
                    <h3>
                        Anja Badeck
                    </h3>
                    <b>
                        künstlerische Leitung
                    </b><br>
                    @auth
                    <a href="mailto:kontakt@anjabadeck.de">
                       kontakt@anjabadeck.de
                    </a>
                    @else
                    <i>Email ist nur für Teams sichtbar.</i>
                    @endauth
                </div>
                <div class="col-md-4">
                    <img src="{{asset("img/team/norman.jpg")}}">
                    <h3>
                        Norman Schenk
                    </h3>
                    <b>
                        Projektmanagement
                    </b><br>
                    <a href="mailto:kontakt@jugendfilmcamp.de">kontakt@jugendfilmcamp.de</a><br>
                    <a href="tel:01608490458">0160 8490458</a><br>
                </div>

                <div class="col-md-4">
                    <img src="{{asset("img/team/jfc.png")}}">
                    <h3>
                        Jugendfilmcamp 
                    </h3>
                    <b>
                        Öffentlichkeitsarbeit & SocialMedia
                    </b><br>
                    <a href="mailto:buero@jugendfilmcamp.de">
                        buero@jugendfilmcamp.de
                    </a>
                </div>
            </div>
            <br/>
            <br/>
            <div style="margin:auto;text-align:center">
                <a href="{{route('links')}}" class="btn btn-lg btn-primary">Wichtige Links <i class="fa fa-external-link"></i></a>
                <a href="{{route('register')}}" class="btn btn-lg btn-primary">Jetzt Team anmelden <i class="fa fa-chevron-right"></i></a>
            </div>
            <br>
        </div>
</div>
@endsection
