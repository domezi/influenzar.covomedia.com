@extends('layouts.app')

@section('content')
<div class="container def">
        <h1 style="">
            Mögliche Themen
        </h1>
        <div class="themen">

        <h2>
            Solidarität 
        </h2>
        Was bedeutet das in Zeiten einer Krise.

        <h2>
            Beteiligung & Engagement 
        </h2>
        Was unternehmt ihr oder Bekannte um zu helfen.
        (Einkaufen, Hilfe mit Haustieren, Erntehilfe…)

        <h2>
            Werte 
        </h2>
        Blicke auf Moral und Egoismus

        <h2>
            Europa
        </h2>
        Was könnte die Zukunft Europas sein? 
        Gemeinschaft vs. Nationalstaaten. 
        Gemeinsames Bekämpfen von Corona.
        Lösungen für Geflüchtete.

        <h2>
            Grenzen
        </h2>
        Offene vs. geschlossene Grenzen?
        Wo beginnen und enden Grenzen? (An der Haustür? Zwischen den Bundesländern? Europäische Grenzen?)
        Hilfreich oder behindernd?
        Reisen ins und Studieren im Ausland. 

        <h2>
            Gerechtigkeit 
        </h2>
        Zugang zu Forschungsergebnissen (Medikamente).
        Krankenversicherung.

        <h2>
            Wertschätzung 
        </h2>
        Z.B. die Bezahlung von systemrelevanten Berufsgruppen.
        Finanzielle solidarische Soforthilfen.

        <h2>
            Zukunft / Entwicklungen
        </h2>
        Kulturschaffende nach der Krise: 
        Was passiert wenn Theater, Filmstudios, Kinos, Museen, Clubs etc. bankrott sind.

        <h2>
            Populismus und Fake-News in Krisenzeiten
        </h2>
        Was ist glaubhaft?
        Wie informiert man sich am besten?
        Ändert sich unser Blick?

        <h2>
            Klimaschutz 
        </h2>
        Ist mit Corona der Kampf um den Klimaschutz beendet?

        <h2>
            Fiktion 
        </h2>
        - ein Leben nach Corona

        <h2>
            Private Schicksale
        </h2>
        Was habt ihr erlebt?

        <h2>
            Kurioses
        </h2>
        Anekdoten, Witze, Musikvideos

        <h2>
            Was bisher geschah?
        </h2>
        Was bisher geschah...
        Dezember 2019 bis Ende März 2020.

        </div>
        <br/>
        <br>
        <a href="{{route('links')}}" class="btn btn-lg btn-primary">Wichtige Links <i class="fa fa-external-link"></i></a>
        <a href="{{route('team')}}" class="btn btn-lg btn-primary">Unsere Ansprechpartner*innen <i class="fa fa-chevron-right"></i></a>

</div>
@endsection
