
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Video hinzufügen</div>
                
                <div class="card-body">
                    <form enctype="multipart/form-data" method="post" action="{{route("videos.store")}}">
                        @csrf

                        <h1>1. Youtube ID</h1>
                            <i>So findest Du die Youtube ID</i><br>
                            <img class="mt-2" src="{{asset('img/explains/youtube_id.png')}}" width=500>
                        <input name="youtube_id" autofocus="true" value="{{old("youtube_id")}}" placeholder="Youtube-ID des Videos" class="mt-2 form-control">
                        @error("youtube_id")
                            <div class="text-danger">{{$message}}</div>
                        @enderror

                        <hr>
                        <h1>2. Video Details</h1>
                        <input name="title" autofocus="true" value="{{old("title")}}" placeholder="Titel des Videos" class="form-control">
                        @error("title")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                 
                        @error("team_id")
                            <div class="text-danger">{{$message}}</div>
                        @enderror
                        <select name="team_id" autofocus="true" value="{{old("team_id")}}" placeholder="Team ID" class="form-control">
                            @foreach($teams as $team)
                            <option {{($team->id == old('team_id') ? ' selected ' : '')}} value="{{$team->id}}">{{$team->title}}</option>
                            @endforeach
                        </select>

                        <hr>
                        <h1>3. Team informieren</h1>
                            <i>Bitte setze bei dem entsprechenden Upload den Status auf "Öffentlich". </i><br>
                            <img class="mt-2" src="{{asset('img/explains/publish_go.png')}}" width=300 style="border:2px dotted gray">
                 
                        <hr>
                        <h1>4. Bestätigen & Online gehen</h1>
                        <i>Bitte prüfe alle eingegeben Daten und bestätige die Freischaltung.</i><br>
                        <input type="submit" value="Video hinzufügen und sofort öffentlich sichtbar schalten!" class="mt-2 btn-block btn btn-primary">
                    
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
