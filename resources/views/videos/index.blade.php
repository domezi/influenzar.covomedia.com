
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">ÖFFENTLICHE Videos</div>
                
                <div class="card-body">
                    <div class="alert alert-danger text-center" style="font-size:1.4em">Alle diese Videos sind <b>öffentlich</b> auf der Webseite sichtbar!</div>
                    <div style="display:flex">
                        <a style="flex:1" href="{{route("videos.create")}}" class="d-block mb-3 btn btn-primary"><i class="fa fa-plus"></i> Video </a>
                    </div>
                    @forelse($videos as $video)
                        <a href="{{route("videos.show",$video->id)}}" class="list-group-item list-group-item-action active">
                            <div class="d-flex w-100 justify-content-between">
                                <img src="https://img.youtube.com/vi/{{$video->youtube_id}}/maxresdefault.jpg" height=100>
                                <div style="flex:1" class="pl-3">
                                    <h5 class="mb-1">{{$video->title}}</h5>
                                    <small>Hochgeladen von {{$video->team->title}}</small>
                                    <br>
                                    <small>Dieses Video ist <b class="text-warning">öffentlich</b> sichtbar!</small>
                                </div>
                                <small>
                                    {{$video->updated_at->diffForHumans()}}
                                </small>
                            </div>
                        </a>
                    @empty
                        Es gibt noch keine Videos hier.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
