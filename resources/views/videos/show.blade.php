
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Details zum Video</div>
                <div class="card-body">
                    <div class="alert alert-danger text-center" style="font-size:1.4em">Dieses Video ist <b>öffentlich</b> auf der Webseite sichtbar!</div>
                    <a href="{{route("videos.index")}}" class="btn btn-block mb-4 btn-primary"><i class="fa fa-chevron-left"></i> Alle Videos</a>
                    <img style="float:left;margin-right:15px" src="https://img.youtube.com/vi/{{$video->youtube_id}}/maxresdefault.jpg" width="300">
                    <h2>Titel: {{$video->title}}</h2>
                    <br>
                    <p>Team: {{$video->team->title}}</p>
                    <p>Thema: {{$video->thema}}</p>
                    <br style="clear:both">
                    <br>
                    <form method="post" action="{{route("videos.delete",$video->id)}}">
                        @csrf
                        @method("delete")
                        <input type="submit" class="btn btn-primary btn-block btn-primary-danger" value="Videoreferenz löschen und SOFORT OFFLINE nehmen !!!">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
