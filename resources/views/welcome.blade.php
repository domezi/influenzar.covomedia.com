@extends('layouts.app')

@section('content')
<div class="container">
    <img src="{{asset("img/home/banner.jpg")}}" width="100%">
    <div style="background-size:cover;background-image:url( {{asset("img/home/blured.jpg")}} );padding:20px 30px;">
        <h1 style="color:white;margin-top:20px">InfluenzAr 2020</h1>
        <div class="row">
        

            <div class="col-md-5">
<iframe width="100%" height="236" src="https://www.youtube.com/embed/PxRDhTi7oiM?autoplay=true" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
            </div>
            <div class="col-md-7">
        <p style="color:white;white-space:pre-line"> Die ganze Welt ist in Aufruhr.
Covid-19, verursacht durch das neuartige Corona Virus, stürzt die Menschheit in eine tiefe Krise.
Scheinbar unaufhaltsam.
Wir als Jugendfilmcamper*innen MÜSSEN reagieren:
So, wie wir sind…
Jung. Kreativ. Kritisch. Aufwühlend. Visionär.
Lasst uns das machen, was uns brennt… filmische Zeitdokumente der jungen Generation zur Krise.        <br/>
        <br/>
        <p>
            </div>
            </div>
        <br/>
        <a href="{{route('team')}}" class="btn btn-lg btn-primary"><i class="fa fa-info-circle"></i> Ansprechpartner </a>
        <a href="{{route('faq')}}" class="btn btn-lg btn-primary">Häufige Fragen <i class="fa fa-chevron-right"></i></a>
    </div>
</div>
@endsection
