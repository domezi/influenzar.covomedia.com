<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () { return view('welcome'); })->name("welcome");
Route::middleware(['writelog'])->group(function () {

Route::get('/',"VideoController@feed")->name("welcome");
Route::get('/feed',"VideoController@feed")->name("feed");

Auth::routes();

Route::middleware(['auth','enabled','admin'])->group(function () {
    Route::get('/admin/metrics/live', 'LogController@index')->name('metrics.live');
    Route::get('/admin/metrics/users/{user}', 'LogController@byUser')->name('metrics.user');
    Route::get('/admin/metrics/routes/{route}', 'LogController@byRoute')->name('metrics.route');
});

Route::middleware(['auth'])->group(function () {

    Route::get('/home', function() {return view("intern/dashboard");})->name('home');

    Route::middleware(['enabled'])->group(function () {

        Route::middleware(['teamer'])->group(function () {
            Route::patch('/teams/wunsch/{team}', 'TeamController@updateThemaWunsch')->name('teams.thema_wunsch.update');

            Route::get('/teams/create', 'TeamController@create')->name('teams.create');
            Route::get('/teams/leave', 'TeamController@leave')->name('team.leave');

            Route::get('/teams/join/{team}', 'TeamController@join')->name('teams.join');
            Route::post('/teams/create', 'TeamController@store')->name('teams.store');
            Route::get('/teams/our-team', 'TeamController@showOurTeam')->name('teams.showOurTeam');
        });

        Route::middleware(['admin'])->group(function () {
            //videos
            Route::get('/admin/videos', 'VideoController@index')->name('videos.index');
            Route::get('/admin/videos/create', 'VideoController@create')->name('videos.create');
            Route::post('/admin/videos/create', 'VideoController@store')->name('videos.store');
            Route::get('/admin/videos/{video}', 'VideoController@show')->name('videos.show');
            Route::delete('/admin/videos/{video}', 'VideoController@delete')->name('videos.delete');

            Route::get('/admin/teams', 'TeamController@adminIndex')->name('teams.index.admin');
            Route::get('/admin/users/{user}/enable/{enable}', 'UserController@setEnabled')->name('users.enable'); // depricated
            Route::patch('/users/{user}', 'UserController@patch')->name('users.patch');
            Route::patch('/team/{team}', 'TeamController@patch')->name('team.patch');
            Route::delete('/teams/{teams}', 'TeamController@delete')->name('teams.delete');
            Route::get('/admin/teams/{team}', 'TeamController@show')->name('teams.show.admin');
            Route::get('/admin/users', 'UserController@index')->name('users.index');
            Route::delete('/users/{user}', 'UserController@delete')->name('users.delete');

            if(getenv("APP_DEBUG")) {
                Route::get('/ad/getAccessToken/','ActiveDirectoryController@getAccessToken');
                Route::get('/ad/createUser/{user}/','ActiveDirectoryController@createUser');
                Route::get('/ad/inviteUser/{user}/','ActiveDirectoryController@inviteUser');
                Route::get('/ad/createGroup/{team}/','ActiveDirectoryController@createGroup');
                Route::get('/ad/updateUser/{user}/','ActiveDirectoryController@updateUser');
            }
            Route::get('/ad/updateUsers','ActiveDirectoryController@updateUsers');
            Route::get('/ad/createTeam/{team}/','ActiveDirectoryController@createTeam');

        });

        Route::middleware(['docent'])->group(function () {
            Route::get('/teams', 'TeamController@index')->name('teams.index');
            Route::get('/teams/{team}', 'TeamController@show')->name('teams.show');
            Route::patch('/teams/{team}', 'TeamController@update')->name('teams.update');
            Route::get('/users/{user}', 'UserController@show')->name('users.show');
            Route::get('/teams/{team}/becomeDocent', 'TeamController@becomeDocent')->name('teams.becomeDocent');
            Route::get('/teams/{team}/freeDocent', 'TeamController@freeDocent')->name('teams.freeDocent');
        });

        Route::get('/teams/{team}/uploads', 'UploadController@index')->name('uploads.index');
        Route::get('/teams/{team}/uploads/create', 'UploadController@create')->name('uploads.create');
        Route::get('/teams/{team}/uploads/createStock', 'UploadController@createStock')->name('uploads.createStock');
        Route::get('/teams/{team}/uploads/createBTS', 'UploadController@createBTS')->name('uploads.createBTS');
        Route::post('/teams/{team}/uploads', 'UploadController@store')->name('uploads.store');

        Route::get('/uploads/tag/{tag}', 'UploadController@indexByTag')->name('uploads.byTag');
        Route::get('/uploads/tags/{tag}', 'UploadController@indexByTags')->name('uploads.byTags');
        Route::post('/uploads/search', 'UploadController@search')->name('uploads.search');
        Route::get('/uploads/search', 'UploadController@search')->name('uploads.search');
        Route::get('/uploads/{upload}', 'UploadController@show')->name('uploads.show');

        Route::patch('/uploads/{upload}', 'UploadController@patch')->name('uploads.patch');

        Route::get('/themen', function() { return view("themen"); })->name('themen');
        Route::get('/briefing', function() { return view("briefing"); })->name('briefing');
        Route::get('/links', function() { return view("links"); })->name('links');
        Route::get('/assets', function() { return view("assets"); })->name('assets');

    });

    Route::get('/reportbug', function() { return view("reportbug"); })->name('reportbug');
    Route::post('/reportbug','ReportBugController@store')->name('reportbug');

    Route::patch('/user', 'UserController@patchSelf')->name('user.patch');
});

Route::get('file-upload', 'FileController@fileUpload');
Route::post('file-upload', 'FileController@fileUploadPost')->name('fileUploadPost');


Route::get('/ansprechpartner', function() { return view("team"); })->name('team');
Route::get('/kontakt', function() { return view("kontakt"); })->name('kontakt');
Route::get('/faq', function() { return view("faq"); })->name('faq');
Route::get('/fes', function() { return view("fes"); })->name('fes');

Route::get('/weeks', function() { return \App\Week::all(); })->name('weeks');

Route::get('/unlisted/uploads/publish',"UploadController@unlistedPublish");

Route::post('/generate-intro', "GeneratorController@generateIntro")->name('generateIntro');
Route::post('/generate-caption', "GeneratorController@generateCaption")->name('generateCaption');


});

