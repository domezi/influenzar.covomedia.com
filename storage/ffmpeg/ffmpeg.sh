#!/bin/bash
ffmpeg -y -i background.mov -filter_complex "[0]split[base][text];[text]drawtext=fontfile=Sahara.ttf:text='$1': fontcolor=white:\
fontsize=140: box=0: boxcolor=black@0.5:boxborderw=5:x=(w-text_w)/2:y=(h-text_h)/2-95,format=yuva444p,fade=t=in:st=0.1:d=1.5:alpha=1,fade=t=out:st=3.6:d=1.7:alpha=1[subtitles]; \
[base][subtitles]overlay" output.mp4

ffmpeg -y -i output.mp4 -filter_complex "[0]split[base][text];[text]drawtext=fontfile=Frutiger.ttf:text='$2': fontcolor=white:\
fontsize=90: box=0: boxcolor=black@0.5:boxborderw=5:x=(w-text_w)/2:y=(h-text_h)/2+60,format=yuva444p,fade=t=in:st=0.8:d=1.9:alpha=1,fade=t=out:st=3.8:d=1.7:alpha=1[subtitles]; \
[base][subtitles]overlay" ../../public/ffmpeg/Euer_Intro.mp4

