#!/bin/bash
ffmpeg -y -i bauchbinde2.mov -filter_complex "[0]split[base][text];[text]drawtext=fontfile=Frutiger.ttf:text='$1': fontcolor=white:\
fontsize=59: box=0: boxcolor=black@0.5:boxborderw=5:x=40:y=885,format=yuva444p,fade=t=in:st=0.8:d=1.4:alpha=1,fade=t=out:st=4:d=1.1:alpha=1[subtitles]; \
[base][subtitles]overlay" output.mp4

ffmpeg -y -i output.mp4 -filter_complex "[0]split[base][text];[text]drawtext=fontfile=Frutiger.ttf:text='$2': fontcolor=white:\
fontsize=41: box=0: boxcolor=black@0.5:boxborderw=5:x=40:y=955,format=yuva444p,fade=t=in:st=1.1:d=1.4:alpha=1,fade=t=out:st=4:d=1.1:alpha=1[subtitles]; \
[base][subtitles]overlay" ../../public/ffmpeg/Bauchbinde.mp4

