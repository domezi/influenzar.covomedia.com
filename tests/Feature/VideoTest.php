<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VideoTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh');
    }

    public function testBasicTest()
    {
        $user = \App\User::create(["name"=>"test","email"=>"mail@test.om","password"=>"hlk"]);
        $this->assertEquals($user->name, "test");
    }

}
